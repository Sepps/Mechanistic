from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps

class Jam(ConanFile):
    name = "Jam"
    description = "A game made for the 2023 game jam."
    url = "https://gitlab.com/Sepps/Jam.git"
    license = "MIT"
    author = "David Ledger (davidledger@live.com.au)"
    version = "0.1.0"
    generators = "CMakeDeps", "CMakeToolchain"
    default_options = {
        "sdl/*:shared": False,
        "sdl/*:iconv": False,
        "sdl_image/*:with_libjpeg": False, # These need libjpeg which doesn't build mingw.
        "sdl_image/*:with_libtiff": False, # These need libjpeg which doesn't build mingw.
        "sdl_image/*:with_libpng": False, # libpng doesn't build with clang + mingw
        "sdl_image/*:shared": False,
        "minizip/*:bzip2": False,
    }
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires("zlib/1.2.13", force=True)
        self.requires("sdl/2.28.3", force=True)
        self.requires("sdl_image/2.0.5")
        self.requires("glad/0.1.36")
        self.requires("glm/cci.20230113")
        self.requires("opengl/system")
        self.requires("concurrentqueue/1.0.3")
        return self.requires

    def build(self):
        cmake = CMake(self, build_type=settings.build_type)
        cmake.configure(source_folder="src")
        cmake.build()
        cmake.install()
