cmake_minimum_required(VERSION 3.22)
project(jam VERSION 0.1.0 LANGUAGES C CXX)

set(JAM_PUBLIC_INC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/inc)
set(JAM_PUBLIC_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(JAM_PRIVATE_INC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/private/inc)
set(JAM_PRIVATE_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/private/src)
file(GLOB JAM_PUBLIC_SRCS CONFIGURE_DEPENDS ${JAM_PUBLIC_SRC_DIR}/*.cpp)
file(GLOB JAM_PRIVATE_SRCS CONFIGURE_DEPENDS ${JAM_PRIVATE_SRC_DIR}/*.cpp)

find_package(glm REQUIRED)
find_package(SDL2 REQUIRED)
find_package(concurrentqueue REQUIRED)

Include(FetchContent)

FetchContent_Declare(
	FnRef
	GIT_REPOSITORY https://gitlab.com/seppeon/fnref.git
	GIT_TAG        1.0.2
)
FetchContent_MakeAvailable(FnRef)

FetchContent_Declare(
	OpCpos
	GIT_REPOSITORY https://gitlab.com/seppeon/libOpCpo.git
	GIT_TAG 	   1.0.0
)
FetchContent_MakeAvailable(OpCpos)

add_library(jam)
target_compile_features(jam
	PUBLIC cxx_std_20
)
target_include_directories(jam
	PUBLIC ${JAM_PUBLIC_INC_DIR}
	PRIVATE ${JAM_PRIVATE_INC_DIR}
)
target_sources(jam 
	PUBLIC ${JAM_PUBLIC_SRCS}
	PRIVATE ${JAM_PRIVATE_SRCS}
)
target_link_libraries(jam
	PUBLIC Hz_Fn glm::glm concurrentqueue::concurrentqueue
	PRIVATE SDL2::SDL2-static
	PUBLIC libOpCpo
)
target_compile_definitions(jam
	PUBLIC SUPPORTS_SDL2
)

if(EMSCRIPTEN)
    set(CMAKE_EXECUTABLE_SUFFIX .html)
	target_compile_options(jam
		PRIVATE
			-pthread
	)
	target_link_options(jam
		PRIVATE
			-pthread
			-sPROXY_TO_PTHREAD
			-sPTHREAD_POOL_SIZE=10
			-sUSE_PTHREADS=1
			-sASSERTIONS=2
	)
endif()