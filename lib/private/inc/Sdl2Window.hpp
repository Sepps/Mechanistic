#pragma once
#include "jam/WindowBase.hpp"
#include "sdl2/Sdl2Init.hpp"
#define SDL_MAIN_HANDLED 
#include <SDL2/SDL.h>

namespace Jam
{
	struct Sdl2Window : WindowBase
	{
		Sdl2Window(std::string title, WindowExtents extents, WindowSettings const & settings = {}) noexcept;

		Sdl2Window& operator=(Sdl2Window &&);
		Sdl2Window& operator=(Sdl2Window const &) = delete;
		Sdl2Window(Sdl2Window &&);
		Sdl2Window(Sdl2Window const &) = delete;

		~Sdl2Window() override;

		[[nodiscard]] WindowType GetWindowType() const override;
		[[nodiscard]] WindowFocus GetWindowFocus() const override;

		[[nodiscard]] std::string GetTitle() const override;
		bool SetTitle(std::string title) override;

		[[nodiscard]] WindowExtents GetExtents() const override;
		bool SetExtents(WindowExtents extents) override;

		[[nodiscard]] WindowDisplayState GetWindowDisplayState() const override;
		bool SetWindowDisplayState(WindowDisplayState state) override;

		[[nodiscard]] WindowFlags GetWindowFlags() const override;
		bool SetWindowFlags(WindowFlags flags) override;

		[[nodiscard]] WindowGraphics GetWindowGraphics() const override;
		bool SetWindowGraphics(WindowGraphics graphics) override;

		bool FlashWindow(WindowFlashType type) override;
		bool RaiseWindow() override;
		bool SetWindowIcon(std::filesystem::path path) override;
		void SwapBuffers() override;
		
		[[nodiscard]] auto GetOpenGlContext() -> opengl_context override;
	private:
		Sdl2VideoSubsystem m_video_subsystem; // must be before window_ptr
		std::unique_ptr<SDL_Window, void(*)(SDL_Window*)> m_window_ptr;
		std::unique_ptr<SDL_Surface, void(*)(SDL_Surface*)> m_icon_ptr;
	};
}