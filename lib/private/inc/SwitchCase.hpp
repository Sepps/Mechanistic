#pragma once
#include "AlwaysInline.hpp"
#include "Fwd.hpp"
#include <functional>
#include <type_traits>
#include <memory>

namespace Hz
{
	template <typename ... Cases>
	struct Switch;

	template<typename K, typename F>
	struct Case;

	template <typename F>
	struct Default;

	template <typename T>
	inline constexpr bool is_switch_v = false;
	template <typename ... Cases>
	inline constexpr bool is_switch_v<Switch<Cases...>> = true;
	template <typename T>
	concept IsSwitch = is_switch_v<std::remove_cvref_t<T>>;

	template <typename T>
	inline constexpr bool is_switch_case_v = false;
	template <typename K, typename F>
	inline constexpr bool is_switch_case_v<Case<K, F>> = true;
	template <typename T>
	concept IsSwitchCase = is_switch_case_v<std::remove_cvref_t<T>>;

	template <typename T>
	inline constexpr bool is_switch_default_v = false;
	template <typename F>
	inline constexpr bool is_switch_default_v<Default<F>> = true;
	template <typename T>
	concept IsSwitchDefault = is_switch_default_v<std::remove_cvref_t<T>>;

	template <typename T>
	concept IsSwitchCaseOrDefault = IsSwitchCase<T> or IsSwitchDefault<T>;

	template <auto V>
	struct Key
	{
		using key_type = decltype(V);
		ALWAYS_INLINE constexpr bool operator==(auto const & key) const noexcept { return key == V; }
	};

	template <typename T>
	struct TypeKey
	{
		using key_type = T;
		ALWAYS_INLINE constexpr bool operator==(key_type const &) const noexcept { return true; }
		ALWAYS_INLINE constexpr bool operator==(auto const & ) const noexcept { return false; }
	};

	template <typename T>
	struct GetKeyTypeImpl { using type = T; };
	template <auto V>
	struct GetKeyTypeImpl<Key<V>> { using type = typename Key<V>::key_type; };
	template <typename T>
	struct GetKeyTypeImpl<TypeKey<T>> { using type = typename TypeKey<T>::key_type; };
	template <typename T>
	using GetKeyType = typename GetKeyTypeImpl<T>::type;

	template <typename F, typename ... Args>
	[[nodiscard]] ALWAYS_INLINE constexpr decltype(auto) SwitchCaseInvoke(F && callback, Args && ... args)
	{
		if constexpr (std::invocable<F, Args...>)
		{
			return std::invoke(FWD(callback), FWD(args)...);
		}
		else if constexpr (std::invocable<F>)
		{
			return std::invoke(FWD(callback));
		}
		else
		{
			return FWD(callback);
		}
	}

	template<typename K, typename F>
	struct Case
	{
		[[no_unique_address]] K key;
		F callback;

		ALWAYS_INLINE constexpr decltype(auto) operator()(auto const & given_key, auto && ... args) const
		{
			if (key == given_key)
			{
				return SwitchCaseInvoke(callback, FWD(args)...);
			}
			std::terminate();
		}
	};
	template<typename K, typename F>
	Case(K, F) -> Case<K, F>;

	template <typename F>
	struct Default
	{
		F callback;

		ALWAYS_INLINE constexpr decltype(auto) operator()(auto const &, auto && ... args) const
		{
			return SwitchCaseInvoke(callback, FWD(args)...);
		}
	};
	template<typename F>
	Default( F ) -> Default<F>;

	template<typename T, typename... Args>
	concept SwitchSameAsAnyOf = ( std::same_as<T, Args> or ... );

	template <typename ... Cases>
	struct Switch;
	template <typename K1, typename F1, typename K2, typename F2>
		requires (SwitchSameAsAnyOf<GetKeyType<K1>, GetKeyType<K2>>)
	struct Switch<Case<K1, F1>, Case<K2, F2>>
	{
		using lhs_type = Case<K1, F1>;
		using rhs_type = Case<K2, F2>;
		using key_type = std::common_type_t<GetKeyType<K1>, GetKeyType<K2>>;

		[[no_unique_address]] lhs_type lhs;
		[[no_unique_address]] rhs_type rhs;

		ALWAYS_INLINE constexpr decltype(auto) operator()(auto const & key, auto && ... args) const
		{
			if (key == lhs.key) return SwitchCaseInvoke(lhs.callback, FWD(args)...);
			if (key == rhs.key) return SwitchCaseInvoke(rhs.callback, FWD(args)...);
			std::terminate();
		}
	};
	template <typename K1, typename F1, typename F2>
	struct Switch<Case<K1, F1>, Default<F2>>
	{
		using lhs_type = Case<K1, F1>;
		using rhs_type = Default<F2>;
		using key_type = GetKeyType<K1>;

		[[no_unique_address]] lhs_type lhs;
		[[no_unique_address]] rhs_type rhs;

		ALWAYS_INLINE constexpr decltype(auto) operator()(auto const & key, auto && ... args) const
		{
			if (key == lhs.key) return SwitchCaseInvoke(lhs.callback, FWD(args)...);
			return rhs(key, FWD(args)...);
		}
	};
	template <typename Key, typename F, typename ...Cases>
	struct Switch<Case<Key, F>, Cases...>
	{
		using lhs_type = Case<Key, F>;
		using rhs_type = Switch<Cases...>;
		using key_type = std::common_type_t<GetKeyType<Key>, typename rhs_type::key_type>;

		[[no_unique_address]] lhs_type lhs;
		[[no_unique_address]] rhs_type rhs;

		ALWAYS_INLINE constexpr decltype(auto) operator()(auto const & key, auto && ... args) const
		{
			return (key == lhs.key) ? SwitchCaseInvoke(lhs.callback, FWD(args)...) : rhs(key, FWD(args)...);
		}
	};

	template <typename K1, typename F1, typename K2, typename F2>
	[[nodiscard]] ALWAYS_INLINE constexpr auto operator|(Case<K1, F1> lhs, Case<K2, F2> rhs) -> Switch<Case<K1, F1>, Case<K2, F2>>
	{
		return Switch<Case<K1, F1>, Case<K2, F2>>{ lhs, rhs };
	}
	template <typename K1, typename F1, typename F2>
	[[nodiscard]] ALWAYS_INLINE constexpr auto operator|(Case<K1, F1> lhs, Default<F2> rhs) -> Switch<Case<K1, F1>, Default<F2>>
	{
		return Switch<Case<K1, F1>, Default<F2>>{ lhs, rhs };
	}
	template <IsSwitchCase ... Cases, IsSwitchCaseOrDefault Rhs>
	[[nodiscard]] ALWAYS_INLINE constexpr auto operator|(Switch<Cases...> lhs, Rhs rhs) -> Switch<Cases..., Rhs>
	{
		return Switch<Cases..., Rhs>
		{
			lhs.lhs,
			( lhs.rhs | rhs )
		};
	}
	template <IsSwitchCase ... LhsCases, IsSwitchCaseOrDefault ... RhsCases>
	[[nodiscard]] ALWAYS_INLINE constexpr auto operator|(Switch<LhsCases...> lhs, Switch<RhsCases...> rhs) -> Switch<LhsCases..., RhsCases...>
	{
		return Switch<LhsCases..., RhsCases...>
		{
			lhs.lhs,
			( lhs.rhs | rhs )
		};
	}
}