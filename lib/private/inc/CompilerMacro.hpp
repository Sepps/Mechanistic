#pragma once

namespace Jam
{
	enum class Compiler
	{
		Microsoft,
		Clang,
		GNU,
		Unknown
	};

#if defined(__clang__)
	#define COMPILED_WITH_CLANG
	inline constexpr Compiler compiler = Compiler::Clang;
#elif defined(_MSC_VER)
	#define COMPILED_WITH_MSVC
	inline constexpr Compiler compiler = Compiler::Microsoft;
#elif defined(__GNUC__)
	#define COMPILED_WITH_GNU
	inline constexpr Compiler compiler = Compiler::GNU;
#else 
	#define COMPILED_WITH_UNKNOWN
	inline constexpr Compiler compiler = Compiler::Unknown;
#endif
}