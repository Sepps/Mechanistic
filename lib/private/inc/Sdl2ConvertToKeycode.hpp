#pragma once
#include "sdl2/Sdl2KeyCodes.hpp"
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_scancode.h>

namespace Jam
{
	[[nodiscard]] KeyCode ConvertToKeycode(SDL_Keycode code) noexcept;
	[[nodiscard]] KeyCode ConvertToKeycode(SDL_Scancode code) noexcept;
}