#pragma once
#include "CompilerMacro.hpp"

#if defined(COMPILED_WITH_MSVC)
#define ALWAYS_INLINE [[msvc::forceinline]] inline
#elif defined(COMPILED_WITH_GNU) or defined(COMPILED_WITH_CLANG)
#define ALWAYS_INLINE [[gnu::always_inline]] inline
#else
// Fallback does nothing.
#define ALWAYS_INLINE inline
#endif