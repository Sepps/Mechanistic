#pragma once
#include <type_traits>

namespace Jam
{
	template <typename T> requires std::is_enum_v<std::remove_cvref_t<T>>
	[[nodiscard]] constexpr auto UnderlyingValue(T const & input) noexcept -> std::underlying_type_t<T>
	{
		return static_cast<std::underlying_type_t<T>>( input );
	}
}