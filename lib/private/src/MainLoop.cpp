#include "jam/MainLoop.hpp"
#include <thread>

#ifdef EMSCRIPTEN
#include <emscripten.h>
namespace Jam
{
	void MainLoop(Hz::Fn::FnRef<void() noexcept> callback) noexcept
	{
		emscripten_set_main_loop_arg(+[](void * ptr){
			auto & ref = *static_cast<Hz::Fn::FnRef<void() noexcept> *>(ptr);
			ref();
		}, &callback, 0, true);
	}
}
#else
namespace Jam
{
	void MainLoop(Hz::Fn::FnRef<void() noexcept> callback) noexcept
	{
		while (true)
		{
			callback();
		}
	}
}
#endif

namespace Jam
{
	void OtherLoop(Hz::Fn::FnRef<void() noexcept> callback) noexcept
	{
		while (true)
		{
			callback();
		}
	}
}