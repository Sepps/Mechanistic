#include "async/Scheduler.hpp"
#include <cassert>
#include <mutex>

namespace Jam
{
	void Scheduler::Push(value_type handle)
	{
		std::unique_lock lock{ m_active_lock};
		m_elems[not m_active_elems].push_back(handle);
	}

	void Scheduler::operator()() noexcept
	{
		for (auto & elem : m_elems[m_active_elems])
		{
			elem();
		}
		m_elems[m_active_elems].clear();
		std::unique_lock lock{ m_active_lock};
		m_active_elems = not m_active_elems;
	}
}