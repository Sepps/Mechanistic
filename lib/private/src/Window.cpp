#include "jam/Window.hpp"
#ifdef SUPPORTS_SDL2
#include "Sdl2Window.hpp"
#endif
#include <utility>
#include <cassert>

namespace Jam
{
	Window::Window(WindowBase * ptr) noexcept
		: m_impl(ptr)
	{
		assert(m_impl != nullptr);
	}

	Window::Window(Window && window)
		: m_impl(std::exchange(window.m_impl, nullptr))
	{}

	Window& Window::operator=(Window && window)
	{
		m_impl = std::exchange(window.m_impl, nullptr);
		return *this;
	}

	WindowType Window::GetWindowType() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetWindowType();
	}
	WindowFocus Window::GetWindowFocus() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetWindowFocus();
	}

	std::string Window::GetTitle() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetTitle();
	}

	bool Window::SetTitle(std::string title)
	{
		assert(m_impl != nullptr);
		return m_impl->SetTitle(title);
	}

	WindowExtents Window::GetExtents() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetExtents();
	}

	bool Window::SetExtents(WindowExtents extents)
	{
		assert(m_impl != nullptr);
		return m_impl->SetExtents(extents);
	}

	WindowDisplayState Window::GetWindowDisplayState() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetWindowDisplayState();
	}

	bool Window::SetWindowDisplayState(WindowDisplayState state)
	{
		assert(m_impl != nullptr);
		return m_impl->SetWindowDisplayState(state);
	}

	WindowFlags Window::GetWindowFlags() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetWindowFlags();
	}

	bool Window::SetWindowFlags(WindowFlags flags)
	{
		assert(m_impl != nullptr);
		return m_impl->SetWindowFlags(flags);
	}

	WindowGraphics Window::GetWindowGraphics() const
	{
		assert(m_impl != nullptr);
		return m_impl->GetWindowGraphics();
	}
	bool Window::SetWindowGraphics(WindowGraphics graphics)
	{
		assert(m_impl != nullptr);
		return m_impl->SetWindowGraphics(graphics);
	}

	bool Window::FlashWindow(WindowFlashType type)
	{
		assert(m_impl != nullptr);
		return m_impl->FlashWindow(type);
	}

	bool Window::RaiseWindow()
	{
		assert(m_impl != nullptr);
		return m_impl->RaiseWindow();
	}

	bool Window::SetWindowIcon(std::filesystem::path path)
	{
		assert(m_impl != nullptr);
		return m_impl->SetWindowIcon(path);
	}

	void Window::SwapBuffers()
	{
		m_impl->SwapBuffers();
	}

	auto Window::GetOpenGlContext() -> WindowBase::opengl_context
	{
		assert(m_impl != nullptr);
		return m_impl->GetOpenGlContext();
	}

	Window CreateWindow(WindowBackend backend, std::string title, WindowExtents extents, WindowSettings const & settings)
	{
		switch (backend)
		{
		#ifdef SUPPORTS_SDL2
		case WindowBackend::Sdl2: return Window(new Sdl2Window(title, extents, settings));
		#endif
		case WindowBackend::Default: return CreateDefaultWindow(title, extents, settings);
		}
		return Window(nullptr);
	}

	Window CreateDefaultWindow(std::string title, WindowExtents extents, WindowSettings const & settings)
	{
		#ifdef SUPPORTS_SDL2
		return CreateWindow(WindowBackend::Sdl2, title, extents, settings);
		#else
		#error "No window libraries supported on this platform."
		#endif
	}
}