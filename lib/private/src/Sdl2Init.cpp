#include "sdl2/Sdl2Init.hpp"
#include "SwitchCase.hpp"
#include "UnderlingValue.hpp"
#include <array>
#include <cassert>

#define SDL_MAIN_HANDLED 
#include <SDL2/SDL.h>

namespace Jam
{
	static constexpr auto flags_mapping = (
		Hz::Case{ Sdl2Subsystem::Timer, SDL_INIT_TIMER } |
		Hz::Case{ Sdl2Subsystem::Audio, SDL_INIT_AUDIO } |
		Hz::Case{ Sdl2Subsystem::Video, SDL_INIT_VIDEO } |
		Hz::Case{ Sdl2Subsystem::Joystick, SDL_INIT_JOYSTICK } |
		Hz::Case{ Sdl2Subsystem::NoParachute, SDL_INIT_NOPARACHUTE }
	);

	auto GetSdl2InitFuncs() noexcept -> Sdl2Funcs const &
	{
		static constexpr auto sdl2_funcs = Sdl2Funcs{
			.init_subsystem = +[](void*, std::uint32_t flags) { return SDL_InitSubSystem(flags); },
			.quit_subsystem = +[](void*, std::uint32_t flags) { return SDL_QuitSubSystem(flags); },
			.quit = +[](void*) { return SDL_Quit(); },
		};
		return sdl2_funcs;
	}

	static std::array<int, sdl2_subsystem_count> enabled_sdl2_subsystems{};

	[[nodiscard]] static int & GetSubsystemCount(Sdl2Subsystem subsystem) noexcept
	{
		auto index = UnderlyingValue(subsystem);
		assert(index < enabled_sdl2_subsystems.size());
		return enabled_sdl2_subsystems[index];
	}

	[[nodiscard]] static bool IncSubsystem(Sdl2Subsystem subsystem) noexcept
	{
		return GetSubsystemCount(subsystem)++ == 0;
	}

	[[nodiscard]] static bool DecSubsystem(Sdl2Subsystem subsystem) noexcept
	{
		return --GetSubsystemCount(subsystem) == 0;
	}

	[[nodiscard]] static bool Sdl2Active() noexcept
	{
		// TODO: should I use SDL_WasInit ?
		return enabled_sdl2_subsystems != std::array<int, sdl2_subsystem_count>{};
	}

	[[nodiscard]] static bool Sdl2Inactive() noexcept
	{
		return not Sdl2Active();
	}

	[[nodiscard]] Uint32 IncInitFlags(std::initializer_list<Sdl2Subsystem> flags) noexcept
	{
		Uint32 subsystem_flags = 0;
		for (auto subsystem : flags)
		{
			if (IncSubsystem(subsystem))
			{
				subsystem_flags |= flags_mapping(subsystem);
			}
		}
		return subsystem_flags;
	}

	[[nodiscard]] Uint32 DecInitFlags(std::array<bool, sdl2_subsystem_count> used_subsystems) noexcept
	{
		Uint32 subsystem_flags = 0;
		for (std::size_t i = 0; i < std::min(sdl2_avaliable_subsystems.size(), used_subsystems.size()); ++i)
		{
			auto subsystem = sdl2_avaliable_subsystems[i];
			auto used = used_subsystems[i];
			if (used and DecSubsystem(subsystem))
			{
				subsystem_flags |= flags_mapping(subsystem);
			}
		}
		return subsystem_flags;
	}

	Sdl2Init::Sdl2Init(std::initializer_list<Sdl2Subsystem> flags, Sdl2Funcs const & sdl2_funcs) noexcept
		: m_sdl2_funcs{ &sdl2_funcs }
	{
		auto already_used = Sdl2Active();
		auto subsystem_flags = IncInitFlags(flags);
		m_initialized = (m_sdl2_funcs->init_subsystem(m_sdl2_funcs->obj_ptr, subsystem_flags) == 0);
		if (m_initialized)
		{
			for (auto subsystem : flags)
			{
				m_used_subsystems[UnderlyingValue(subsystem)] = true;
			}
		}
	}
	Sdl2Init::~Sdl2Init() noexcept
	{
		auto subsystem_flags = DecInitFlags(m_used_subsystems);
		if (UsesSubsystem())
		{
			m_sdl2_funcs->quit_subsystem(m_sdl2_funcs->obj_ptr, subsystem_flags);
		}
		if (Sdl2Inactive())
		{
			m_sdl2_funcs->quit(m_sdl2_funcs->obj_ptr);
		}
	}

	bool Sdl2Init::IsTimerEnabled() const noexcept
	{
		return enabled_sdl2_subsystems[UnderlyingValue(Sdl2Subsystem::Timer)] > 0;
	}
	bool Sdl2Init::IsAudioEnabled() const noexcept
	{
		return enabled_sdl2_subsystems[UnderlyingValue(Sdl2Subsystem::Audio)] > 0;
	}
	bool Sdl2Init::IsVideoEnabled() const noexcept
	{
		return enabled_sdl2_subsystems[UnderlyingValue(Sdl2Subsystem::Video)] > 0;
	}
	bool Sdl2Init::IsJoystickEnabled() const noexcept
	{
		return enabled_sdl2_subsystems[UnderlyingValue(Sdl2Subsystem::Joystick)] > 0;
	}
	bool Sdl2Init::IsNoParachuteEnabled() const noexcept
	{
		return enabled_sdl2_subsystems[UnderlyingValue(Sdl2Subsystem::NoParachute)] > 0;
	}
	bool Sdl2Init::UsesSubsystem() const noexcept
	{
		return m_used_subsystems != decltype(m_used_subsystems){};
	}
	void Sdl2Init::SetSdl2Funcs(Sdl2Funcs const * const sdl2_funcs) noexcept
	{
		assert(sdl2_funcs != nullptr);
		m_sdl2_funcs = sdl2_funcs;
	}
}