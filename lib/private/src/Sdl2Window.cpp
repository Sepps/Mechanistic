#include "Sdl2Window.hpp"

#include "SwitchCase.hpp"
#include "sdl2/Sdl2Init.hpp"

namespace Jam
{
	namespace
	{
		struct SdlWindow
		{
			SDL_Window * window_ptr = nullptr;
			SDL_Surface * icon_ptr = nullptr;
			Sdl2Init sdl2_init{ Sdl2Subsystem::Video };
		};

		Uint32 BuildFlags(WindowSettings const & settings) noexcept
		{
			Uint32 output = 0;

			static constexpr auto get_window_type_flags = (
				Hz::Case{ WindowType::Normal, Uint32{} } |
				Hz::Case{ WindowType::PopupMenu, SDL_WINDOW_POPUP_MENU } |
				Hz::Case{ WindowType::Tooltip, SDL_WINDOW_TOOLTIP } |
				Hz::Case{ WindowType::Utility, SDL_WINDOW_UTILITY }
			);
			output |= get_window_type_flags(settings.type);

			if (settings.flags.hidden) output |= SDL_WINDOW_HIDDEN;
			if (settings.flags.fullscreen) output |= SDL_WINDOW_FULLSCREEN;
			if (settings.flags.always_on_top) output |= SDL_WINDOW_ALWAYS_ON_TOP;
			if (settings.flags.borderless) output |= SDL_WINDOW_BORDERLESS;
			if (settings.flags.resizable) output |= SDL_WINDOW_RESIZABLE;
			if (settings.flags.mouse_capture) output |= SDL_WINDOW_MOUSE_CAPTURE;
			if (settings.flags.skip_taskbar) output |= SDL_WINDOW_SKIP_TASKBAR;
			if (settings.flags.mouse_grabbed) output |= SDL_WINDOW_MOUSE_GRABBED;
			if (settings.flags.keyboard_grabbed) output |= SDL_WINDOW_KEYBOARD_GRABBED;

			static constexpr auto get_display_state_flags = (
				Hz::Case{ WindowDisplayState::Restored, Uint32{} } | 
				Hz::Case{ WindowDisplayState::Minimised, SDL_WINDOW_MINIMIZED } |
				Hz::Case{ WindowDisplayState::Maximised, SDL_WINDOW_MAXIMIZED }
			);

			if (settings.graphics.opengl) output |= SDL_WINDOW_OPENGL;
			if (settings.graphics.vulkan) output |= SDL_WINDOW_VULKAN;

			return output;
		}

		Uint32 GetWindowFlags(SDL_Window * window_ptr) noexcept
		{
			return SDL_GetWindowFlags(window_ptr);
		}
	}

	Sdl2Window::Sdl2Window(std::string title, WindowExtents extents, WindowSettings const & settings) noexcept
		: m_window_ptr(
			SDL_CreateWindow(
				title.data(),
				// TODO: Interval check.
				extents.x.centered ? SDL_WINDOWPOS_CENTERED : static_cast<int>(extents.x.value),
				extents.y.centered ? SDL_WINDOWPOS_CENTERED : static_cast<int>(extents.y.value),
				static_cast<int>(extents.width),
				static_cast<int>(extents.height),
				BuildFlags(settings)
			),
			&SDL_DestroyWindow
		)
		, m_icon_ptr( nullptr, &SDL_FreeSurface )
	{}
	Sdl2Window::~Sdl2Window()
	{
	}

	std::string Sdl2Window::GetTitle() const
	{
		return std::string( SDL_GetWindowTitle(m_window_ptr.get()) );
	}
	bool Sdl2Window::SetTitle(std::string name)
	{
		SDL_SetWindowTitle(m_window_ptr.get(), name.data());
		return true;
	}
	
	WindowExtents Sdl2Window::GetExtents() const
	{
		int x = 0;
		int y = 0;
		SDL_GetWindowPosition(m_window_ptr.get(), &x, &y);

		int w = 0;
		int h = 0;
		SDL_GetWindowSize(m_window_ptr.get(), &w, &h);

		// Note these can fail in SDL3.
		return {
			static_cast<std::uint32_t>(x),
			static_cast<std::uint32_t>(w),
			static_cast<std::uint32_t>(y),
			static_cast<std::uint32_t>(h)
		};
	}
	bool Sdl2Window::SetExtents(WindowExtents extents)
	{
		SDL_SetWindowPosition(m_window_ptr.get(),
			extents.x.centered ? SDL_WINDOWPOS_CENTERED : static_cast<int>(extents.x.value),
			extents.y.centered ? SDL_WINDOWPOS_CENTERED : static_cast<int>(extents.y.value)
		);
		SDL_SetWindowSize(m_window_ptr.get(), 
			static_cast<int>(extents.width),
			static_cast<int>(extents.height)
		);
		return true;
	}

	WindowType Sdl2Window::GetWindowType() const
	{
		auto flags = Jam::GetWindowFlags(m_window_ptr.get());
		auto masked_flags = flags & (SDL_WINDOW_TOOLTIP | SDL_WINDOW_UTILITY | SDL_WINDOW_POPUP_MENU);
		if (masked_flags == SDL_WINDOW_TOOLTIP) return WindowType::Tooltip;
		if (masked_flags == SDL_WINDOW_UTILITY) return WindowType::Utility;
		if (masked_flags == SDL_WINDOW_POPUP_MENU) return WindowType::PopupMenu;
		return WindowType::Normal;
	}

	WindowDisplayState Sdl2Window::GetWindowDisplayState() const
	{
		auto flags = Jam::GetWindowFlags(m_window_ptr.get());
		auto masked_flags = flags & (SDL_WINDOW_MAXIMIZED | SDL_WINDOW_MINIMIZED);
		if (masked_flags == SDL_WINDOW_MAXIMIZED) return WindowDisplayState::Maximised;
		if (masked_flags == SDL_WINDOW_MINIMIZED) return WindowDisplayState::Minimised;
		return WindowDisplayState::Restored;
	}
	bool Sdl2Window::SetWindowDisplayState(WindowDisplayState state)
	{
		auto action = (
			Hz::Case{ WindowDisplayState::Maximised, [&]{ SDL_MaximizeWindow(m_window_ptr.get()); } } |
			Hz::Case{ WindowDisplayState::Minimised, [&]{ SDL_MinimizeWindow(m_window_ptr.get()); } } |
			Hz::Case{ WindowDisplayState::Restored, [&]{ SDL_RestoreWindow(m_window_ptr.get()); } }
		);
		action(state);
		// TODO: Can likely fail in SDL3
		return true;
	}

	WindowFlags Sdl2Window::GetWindowFlags() const
	{
		auto flags = Jam::GetWindowFlags(m_window_ptr.get());
		WindowFlags output{};
		output.hidden = ((flags & SDL_WINDOW_HIDDEN) != 0);
		output.skip_taskbar = ((flags & SDL_WINDOW_SKIP_TASKBAR) != 0);
		output.mouse_capture = ((flags & SDL_WINDOW_MOUSE_CAPTURE) != 0);
		output.fullscreen = ((flags & SDL_WINDOW_FULLSCREEN) != 0);
		output.always_on_top = ((flags & SDL_WINDOW_ALWAYS_ON_TOP) != 0);
		output.borderless = ((flags & SDL_WINDOW_BORDERLESS) != 0);
		output.resizable = ((flags & SDL_WINDOW_RESIZABLE) != 0);
		output.mouse_grabbed = ((flags & SDL_WINDOW_MOUSE_GRABBED) != 0);
		output.keyboard_grabbed = ((flags & SDL_WINDOW_KEYBOARD_GRABBED) != 0);
		return output;
	}
	bool Sdl2Window::SetWindowFlags(WindowFlags flags)
	{
		auto old = GetWindowFlags();
		// TODO: Add rollback of settinsg on failure...
		bool success = true;
		success &= old.skip_taskbar == flags.skip_taskbar;
		success &= old.mouse_capture == flags.mouse_capture;
		success &= SDL_SetWindowFullscreen(m_window_ptr.get(),   flags.fullscreen ? SDL_TRUE : SDL_FALSE) == 0;
		(flags.hidden ? SDL_HideWindow(m_window_ptr.get()) : SDL_ShowWindow(m_window_ptr.get()));
		SDL_SetWindowAlwaysOnTop(m_window_ptr.get(),  flags.always_on_top ? SDL_TRUE : SDL_FALSE);
		SDL_SetWindowBordered(m_window_ptr.get(),     (not flags.borderless) ? SDL_TRUE : SDL_FALSE);
		SDL_SetWindowResizable(m_window_ptr.get(),    flags.resizable ? SDL_TRUE : SDL_FALSE);
		SDL_SetWindowMouseGrab(m_window_ptr.get(),    flags.mouse_grabbed ? SDL_TRUE : SDL_FALSE);
		SDL_SetWindowKeyboardGrab(m_window_ptr.get(), flags.keyboard_grabbed ? SDL_TRUE : SDL_FALSE);
		return success;
	}

	WindowGraphics Sdl2Window::GetWindowGraphics() const
	{
		auto flags = Jam::GetWindowFlags(m_window_ptr.get());
		WindowGraphics output{};
		output.opengl = ((flags & SDL_WINDOW_OPENGL) != 0);
		output.vulkan = ((flags & SDL_WINDOW_VULKAN) != 0);
		return output;
	}

	bool Sdl2Window::SetWindowGraphics(WindowGraphics graphics)
	{
		auto old = GetWindowGraphics();
		bool success = true;

		if (graphics.opengl and not old.opengl)
		{
			success &= SDL_GL_LoadLibrary(NULL) == 0;
		}
		if (not graphics.opengl and old.opengl)
		{
			SDL_GL_UnloadLibrary();
		}

		return success;
	}

	WindowFocus Sdl2Window::GetWindowFocus() const
	{
		auto flags = Jam::GetWindowFlags(m_window_ptr.get());
		return {
			.input_focus = (flags & SDL_WINDOW_INPUT_FOCUS) != 0,
			.mouse_focus = (flags & SDL_WINDOW_MOUSE_FOCUS) != 0,
		};
	}

	bool Sdl2Window::FlashWindow(WindowFlashType type)
	{
		static constexpr auto translate = (
			Hz::Case{ WindowFlashType::Briefly, SDL_FlashOperation::SDL_FLASH_BRIEFLY } |
			Hz::Case{ WindowFlashType::Cancel, SDL_FlashOperation::SDL_FLASH_CANCEL } |
			Hz::Case{ WindowFlashType::UntilFocused, SDL_FlashOperation::SDL_FLASH_UNTIL_FOCUSED }
		);
		return SDL_FlashWindow(m_window_ptr.get(), translate(type)) == 0;
	}

	bool Sdl2Window::RaiseWindow()
	{
		SDL_RaiseWindow(m_window_ptr.get());
		return true;
	}

	bool Sdl2Window::SetWindowIcon(std::filesystem::path path)
	{
		// auto * surface_ptr = IMG_Load(path.string().data());
		// if (surface_ptr == nullptr) return false;
		// m_icon_ptr.reset(surface_ptr);
		// SDL_SetWindowIcon(m_window_ptr.get(), surface_ptr);
		return false;
	}

	void Sdl2Window::SwapBuffers()
	{
		SDL_GL_SwapWindow(m_window_ptr.get());
	}

	auto Sdl2Window::GetOpenGlContext() -> opengl_context
	{
		return opengl_context{ SDL_GL_CreateContext(m_window_ptr.get()), &SDL_GL_DeleteContext };
	}
}