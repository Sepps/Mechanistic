#include "cst/Cst.hpp"
#include <algorithm>

namespace Cst
{
	XSplit<Rect> Rect::SplitOn(XValue value) const noexcept
	{
		auto const value_lim = std::min(value, rect.width);
		return XSplit<Rect>{
			.left = {
				.pt = pt,
				.rect = {
					.width = value_lim,
					.height = rect.height,
				},
			},
			.right = {
				.pt = {
					.x = pt.x + value_lim,
					.y = pt.y,
				},
				.rect = {
					.width = rect.width - value_lim,
					.height = rect.height,
				},
			},
		};
	}

	YSplit<Rect> Rect::SplitOn(YValue value) const noexcept
	{
		auto const value_lim = std::min(value, rect.height);
		return YSplit<Rect>{
			.bottom = {
				.pt = pt,
				.rect = {
					.width = rect.width,
					.height = value_lim,
				},
			},
			.top = {
				.pt = {
					.x = pt.x,
					.y = pt.y + value_lim,
				},
				.rect = {
					.width = rect.width,
					.height = rect.height - value_lim,
				},
			},
		};
	}

	Rect Rect::ExtendFromLeft(XValue value) const noexcept
	{
		return Rect{
			.pt = { pt.x - value, pt.y },
			.rect = { value, pt.y },
		};
	}

	Rect Rect::ExtendFromRight(XValue value) const noexcept
	{
		return Rect{
			.pt = { pt.x + rect.width, pt.y },
			.rect = { value, pt.y },
		};
	}

	Rect Rect::ExtendFromTop(YValue value) const noexcept
	{
		return Rect{
			.pt = { pt.x, pt.y + rect.height },
			.rect = { pt.x, value },
		};
	}

	Rect Rect::ExtendFromBottom(YValue value) const noexcept
	{
		return Rect{
			.pt = { pt.x, pt.y - value },
			.rect = { pt.x, value },
		};
	}

	Rect Rect::Outset(XValue x_amount, YValue y_amount) const noexcept
	{
		auto const x_amount_lim = XValue{std::abs(x_amount.value)};
		auto const y_amount_lim = YValue{std::abs(y_amount.value)};
		return Offset(x_amount_lim, x_amount_lim, y_amount_lim, y_amount_lim);
	}

	Rect Rect::Inset(XValue x_amount, YValue y_amount) const noexcept
	{
		auto const x_amount_lim = std::min(XValue{std::abs(x_amount.value)}, rect.width / XValue{2});
		auto const y_amount_lim = std::min(YValue{std::abs(y_amount.value)}, rect.height / YValue{2});
		return Offset(-x_amount_lim, -x_amount_lim, -y_amount_lim, -y_amount_lim);
	}

	Rect Rect::Offset(XValue left, XValue right, YValue top, YValue bottom) const noexcept
	{
		return Rect{
			.pt = {
				.x = pt.x - left,
				.y = pt.y - bottom,
			},
			.rect = {
				.width = rect.width + (left + right),
				.height = rect.height + (bottom + top),
			},
		};
	}
}