#include "sdl2/Sdl2Event.hpp"
#include "sdl2/Sdl2AppEvent.hpp"
#include "sdl2/Sdl2ControllerEvent.hpp"
#include "sdl2/Sdl2EventCommon.hpp"
#include "Sdl2ConvertToKeycode.hpp"
#include "sdl2/Sdl2JoyStickEvent.hpp"
#include "sdl2/Sdl2KeyEvent.hpp"
#include "sdl2/Sdl2MouseEvent.hpp"
#include "sdl2/Sdl2SensorEvent.hpp"
#include "sdl2/Sdl2TextEvent.hpp"
#include "sdl2/Sdl2TouchEvent.hpp"
#include "sdl2/Sdl2TouchPadState.hpp"
#include "SwitchCase.hpp"

#include <iostream>
#include <chrono>
#include <exception>
#include <thread>
#include <utility>
#include <ranges>

#ifdef SUPPORTS_SDL2
#define SDL_MAIN_HANDLED 
#include <SDL_stdinc.h>
#include <SDL_mouse.h>
#include <SDL_keyboard.h>
#include <SDL_keycode.h>
#include <SDL_scancode.h>
#include <SDL_timer.h>
#include <SDL_video.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

namespace Jam
{
	namespace
	{
		inline constexpr auto display_event_id_to_display_event_type = (
			Hz::Case(SDL_DisplayEventID::SDL_DISPLAYEVENT_ORIENTATION, 		DisplayEventType::Orientation) |
			Hz::Case(SDL_DisplayEventID::SDL_DISPLAYEVENT_CONNECTED, 		DisplayEventType::Connected) |
			Hz::Case(SDL_DisplayEventID::SDL_DISPLAYEVENT_DISCONNECTED, 	DisplayEventType::Disconnected)
		);
		inline constexpr auto window_event_id_to_window_event_type = (
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_SHOWN, 			[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Shown{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_HIDDEN, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Hidden{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_EXPOSED, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Exposed{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_MOVED, 			[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Moved{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_RESIZED, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Resized{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_SIZE_CHANGED, 	[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::SizeChanged{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_MINIMIZED, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Minimized{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_MAXIMIZED, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Maximized{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_RESTORED, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Restored{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_ENTER, 			[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Enter{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_LEAVE, 			[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Leave{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_FOCUS_GAINED, 	[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::FocusGained{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_FOCUS_LOST, 	[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::FocusLost{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_CLOSE, 			[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::Close{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_TAKE_FOCUS, 	[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::TakeFocus{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_HIT_TEST, 		[]() -> typename WindowEvent::event_data_type{ return typename WindowEvent::HitTest{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_ICCPROF_CHANGED, []() -> typename WindowEvent::event_data_type{ return typename WindowEvent::IccprofChanged{}; }) |
			Hz::Case(SDL_WindowEventID::SDL_WINDOWEVENT_DISPLAY_CHANGED, []() -> typename WindowEvent::event_data_type{ return typename WindowEvent::DisplayChanged{}; })
		);

		struct FreeOnExit{ char * ptr; ~FreeOnExit() { SDL_free(ptr); } };

		Event TranslateQuit(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.quit.timestamp) },
				.event_data{ AppEvent::Quit },
			};
		}
		Event TranslateAppTerminating(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::Terminating },
			};
		}
		Event TranslateFirstEvent(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data = AppEvent::Error
			};
		}
		Event TranslateAppLowMemory(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::LowMemory },
			};
		}
		Event TranslateAppWillEnterBackground(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::AppWillEnterBackground },
			};
		}
		Event TranslateAppDidEnterBackground(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::AppDidEnterBackground },
			};
		}
		Event TranslateAppWillEnterForeground(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::AppWillEnterForeground },
			};
		}
		Event TranslateAppDidEnterForeground(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(static_cast<std::uint64_t>(event.common.timestamp)) },
				.event_data{ AppEvent::AppDidEnterForeground },
			};
		}
		Event TranslateLocaleChanged(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.common.timestamp) },
				.event_data{ AppEvent::LocaleChanged },
			};
		}
		Event TranslateDisplayEvent(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.display.timestamp) },
				.event_data{ DisplayEvent{
					.display_index = static_cast<std::uint32_t>(event.display.display),
					.event_type = display_event_id_to_display_event_type(event.display.type),
				}}
			};
		}
		Event TranslateWindowEvent(SDL_Event const & event) noexcept
		{
			auto const & e = event.window;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = static_cast<std::uint32_t>(e.windowID),
					.event_data = window_event_id_to_window_event_type(e.event),
				}
			};
		}
		KeyModifiers ExtractModifiers(SDL_Keysym const & keysym) noexcept
		{
			auto modifier_set = [mod = keysym.mod](SDL_Keymod keymod) noexcept -> bool
			{
				return (mod & keymod) == keymod;
			};
			return KeyModifiers{
				.state = KeyStateModifiers{
					.shift{
						.left = modifier_set(SDL_Keymod::KMOD_LSHIFT),
						.right = modifier_set(SDL_Keymod::KMOD_RSHIFT),
					},
					.ctrl{
						.left = modifier_set(SDL_Keymod::KMOD_LCTRL),
						.right = modifier_set(SDL_Keymod::KMOD_RCTRL),
					},
					.alt{
						.left = modifier_set(SDL_Keymod::KMOD_LALT),
						.right = modifier_set(SDL_Keymod::KMOD_RALT),
					},
					.os{
						.left = modifier_set(SDL_Keymod::KMOD_LGUI),
						.right = modifier_set(SDL_Keymod::KMOD_RGUI),
					}
				},
				.toggle = KeyToggleModifiers{
					.num_lock = modifier_set(SDL_Keymod::KMOD_NUM),
					.caps_lock = modifier_set(SDL_Keymod::KMOD_CAPS),
					.scroll_lock = modifier_set(SDL_Keymod::KMOD_SCROLL),
				}
			};
		}
		Event TranslateKeyDown(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.key.timestamp) },
				.event_data = WindowEvent{ 
					.window_id = event.key.windowID,
					.event_data = KeyEvent{
						.key_state = false,
						.is_repeat = (event.key.repeat > 0),
						.modifiers = ExtractModifiers(event.key.keysym),
						.key_code = ConvertToKeycode(event.key.keysym.sym),
						.scan_code = ConvertToKeycode(event.key.keysym.scancode),
					}
				}
			};
		}
		Event TranslateKeyUp(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.key.timestamp) },
				.event_data = WindowEvent{ 
					.window_id = event.key.windowID,
					.event_data = KeyEvent{
						.key_state = true,
						.is_repeat = (event.key.repeat > 0),
						.modifiers = ExtractModifiers(event.key.keysym),
						.key_code = ConvertToKeycode(event.key.keysym.sym),
						.scan_code = ConvertToKeycode(event.key.keysym.scancode),
					}
				}
			};
		}
		std::u8string ToUtf8String(char const * ptr) noexcept
		{
			return ptr ? std::u8string(reinterpret_cast<char8_t const *>(ptr)) : std::u8string();
		}
		WindowEvent ExtractTextEditing(auto const & e) noexcept
		{
			return WindowEvent{
				.window_id = e.windowID,
				.event_data = TextEvent{
					.text = ToUtf8String(e.text),
					.event_data = TextEvent::Editing{
						.start = static_cast<std::size_t>(e.start),
						.length = static_cast<std::size_t>(e.length),
					}
				}
			};
		}
		Event TranslateTextEditing(SDL_Event const & event) noexcept
		{
			auto const & e = event.edit;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ExtractTextEditing(e),
			};
		}
		Event TranslateTexteditingExt(SDL_Event const & event) noexcept
		{
			auto const & e = event.editExt;
			FreeOnExit auto_free(e.text);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ExtractTextEditing(e),
			};
		}
		Event TranslateTextInput(SDL_Event const & event) noexcept
		{
			auto const & e = event.text;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = TextEvent{
						.text = ToUtf8String(e.text),
						.event_data = TextEvent::Input{}
					}
				}
			};
		}
		Event TranslateKeymapChanged(SDL_Event const & event) noexcept
		{
			return Event{
				.timestamp{ std::chrono::milliseconds(event.common.timestamp) },
				.event_data = AppEvent::KeymapChanged,
			};
		}
		Event TranslateMouseMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.motion;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = MouseEvent{
						.mouse_instance_id = e.which,
						.coordinate = EventVec2d
						{
							e.x,
							e.y,
						},
						.event_data = MouseEvent::Motion
						{{
							e.xrel,
							e.yrel,
						}}
					}
				}
			};
		}
		MouseButton TranslateMouseButton(std::uint8_t button) noexcept
		{
			return (
				Hz::Case{ SDL_BUTTON_LEFT, MouseButton::Left} |
				Hz::Case{ SDL_BUTTON_MIDDLE, MouseButton::Middle} |
				Hz::Case{ SDL_BUTTON_RIGHT, MouseButton::Right} |
				Hz::Case{ SDL_BUTTON_X1, MouseButton::X1} |
				Hz::Case{ SDL_BUTTON_X2, MouseButton::X2}
			)(button);
		}

		using mouse_button_event_type = decltype(MouseEvent::event_data);
		WindowEvent Translate(SDL_MouseButtonEvent const & e) noexcept
		{
			return WindowEvent{
				.window_id = e.windowID,
				.event_data = MouseEvent
				{
					.mouse_instance_id = e.which,
					.coordinate = {
						e.x,
						e.y,
					},
					.event_data = decltype(MouseEvent::event_data){
						
					}
				}
			};
		}
		Event TranslateMouseButtonDown(SDL_Event const & event) noexcept
		{
			auto const & e = event.button;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = Translate(e),
			};
		}
		Event TranslateMouseButtonUp(SDL_Event const & event) noexcept
		{
			auto const & e = event.button;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = Translate(e),
			};
		}
		Event TranslateMouseWheel(SDL_Event const & event) noexcept
		{
			auto const & e = event.wheel;
			int direction = ((e.direction == SDL_MOUSEWHEEL_NORMAL ) ? 1 : -1);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = MouseEvent{
						.mouse_instance_id = e.which,
						.coordinate = {
#if SDL_VERSION_ATLEAST(2,26,5)
							e.mouseX,
							e.mouseY,
#else
							[]
							{
								int x, y;
								SDL_GetMouseState(&x, &y);
								return EventVec2d{x, y};
							}()
#endif
						},
						.event_data = MouseEvent::Wheel{{
							e.preciseX * direction,
							e.preciseY * direction,
						}},
					}
				}
			};
		}
		Event TranslateJoyaxisMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.jaxis;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = JoyStickEvent::JoyaxisMotion{
						.axis = e.axis,
						.value = e.value,
					},
				},
			};
		}
		Event TranslateJoyballMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.jball;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = JoyStickEvent::JoyballMotion
					{
						.ball_id = e.ball,
						.relative_motion = EventVec2d{
							e.xrel,
							e.yrel,
						}
					}
				}
			};
		}
		JoyStickEvent::JoyhatMotionPosition TranslateMotionPosition(std::uint8_t value) noexcept
		{
			switch (value)
			{
			case SDL_HAT_CENTERED: 	return JoyStickEvent::JoyhatMotionPosition::Centered;
			case SDL_HAT_UP: 		return JoyStickEvent::JoyhatMotionPosition::Up;
			case SDL_HAT_RIGHT: 	return JoyStickEvent::JoyhatMotionPosition::Right;
			case SDL_HAT_DOWN: 		return JoyStickEvent::JoyhatMotionPosition::Down;
			case SDL_HAT_LEFT: 		return JoyStickEvent::JoyhatMotionPosition::Left;
			case SDL_HAT_RIGHTUP: 	return JoyStickEvent::JoyhatMotionPosition::RightUp;
			case SDL_HAT_RIGHTDOWN: return JoyStickEvent::JoyhatMotionPosition::RightDown;
			case SDL_HAT_LEFTUP: 	return JoyStickEvent::JoyhatMotionPosition::LeftUp;
			case SDL_HAT_LEFTDOWN: 	return JoyStickEvent::JoyhatMotionPosition::LeftDown;
			default: 				return JoyStickEvent::JoyhatMotionPosition::Centered;
			}
		}
		Event TranslateJoyhatMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.jhat;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent
				{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = JoyStickEvent::JoyhatMotion
					{
						.hat_id = e.hat,
						.motion_position = TranslateMotionPosition(e.value),
					}
				},
			};
		}
		JoyStickEvent TranslateJoyButton(SDL_JoyButtonEvent const & e) noexcept
		{
			return JoyStickEvent
			{
				.joystick_id = static_cast<std::uint32_t>(e.which),
				.event_data = Button{
					.id = e.button,
					.count = 1,
					.state = (e.state == SDL_PRESSED),
				}
			};
		}
		Event TranslateJoyButtonDown(SDL_Event const & event) noexcept
		{
			auto const & e = event.jbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TranslateJoyButton(e),
			};
		}
		Event TranslateJoyButtonUp(SDL_Event const & event) noexcept
		{
			auto const & e = event.jbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TranslateJoyButton(e),
			};
		}
		Event TranslateJoyDeviceAdded(SDL_Event const & event) noexcept
		{
			auto const & e = event.jdevice;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = JoyStickEvent::JoyDeviceAdded{},
				},
			};
		}
		Event TranslateJoyDeviceRemoved(SDL_Event const & event) noexcept
		{
			auto const & e = event.jdevice;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = JoyStickEvent::JoyDeviceRemoved{},
				},
			};
		}
		Event TranslateJoyBatteryUpdated(SDL_Event const & event) noexcept
		{
			using Hz::Case;
			auto const & e = event.jbattery;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = JoyStickEvent{
					.joystick_id = static_cast<std::uint32_t>(e.which),
					.event_data = (
						Case{ SDL_JOYSTICK_POWER_EMPTY, JoyStickEvent::JoyStickPowerLevel::Empty } |
						Case{ SDL_JOYSTICK_POWER_LOW, JoyStickEvent::JoyStickPowerLevel::Low } |
						Case{ SDL_JOYSTICK_POWER_MEDIUM, JoyStickEvent::JoyStickPowerLevel::Medium } |
						Case{ SDL_JOYSTICK_POWER_FULL, JoyStickEvent::JoyStickPowerLevel::Full } |
						Case{ SDL_JOYSTICK_POWER_WIRED, JoyStickEvent::JoyStickPowerLevel::Wired } |
						Case{ SDL_JOYSTICK_POWER_MAX, JoyStickEvent::JoyStickPowerLevel::Max }
					)(e.level)
				}
			};
		}
		Event TranslateControllerAxisMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.caxis;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::AxisMotion{
						.axis = e.axis,
						.value = e.value,
					}
				}
			};
		}
		Event TranslateControllerButtonDown(SDL_Event const & event) noexcept
		{
			auto const & e = event.cbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = Button{
						.id = e.button,
						.count = 1,
						.state = true,
					}
				}
			};
		}
		Event TranslateControllerButtonUp(SDL_Event const & event) noexcept
		{
			auto const & e = event.cbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = Button{
						.id = e.button,
						.count = 1,
						.state = false,
					}
				}
			};
		}
		Event TranslateControllerDeviceAdded(SDL_Event const & event) noexcept
		{
			auto const & e = event.cbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::Device::Added,
				}
			};
		}
		Event TranslateControllerDeviceRemoved(SDL_Event const & event) noexcept
		{
			auto const & e = event.cbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::Device::Removed,
				}
			};
		}
		Event TranslateControllerDeviceRemapped(SDL_Event const & event) noexcept
		{
			auto const & e = event.cbutton;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::Device::Remapped,
				}
			};
		}
		ControllerEvent TranslateControllerTouchPadEvent(SDL_ControllerTouchpadEvent const & e, bool touch_state) noexcept
		{
			return ControllerEvent{
				.controller_id = static_cast<std::uint32_t>(e.which),
				.event_data = ControllerEvent::TouchState{
					.touch_id = static_cast<std::uint32_t>(e.touchpad),
					.touch_pad_state = TouchPadState{
						.finger_id = static_cast<std::uint32_t>(e.finger),
						.coordinate = {
							e.x,
							e.y,
						},
						.pressure = e.pressure,
						.touch_state = touch_state,
					}
				}
			};
		}
		Event TranslateControllerTouchPadDown(SDL_Event const & event) noexcept
		{
			auto const & e = event.ctouchpad;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TranslateControllerTouchPadEvent(e, true),
			};
		}
		Event TranslateControllerTouchPadUp(SDL_Event const & event) noexcept
		{
			auto const & e = event.ctouchpad;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TranslateControllerTouchPadEvent(e, true),
			};
		}
		Event TranslateControllerTouchPadMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.ctouchpad;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::TouchMotion{
						.touch_id = static_cast<std::uint32_t>(e.touchpad),
						.touch_pad_motion = TouchPadMotion{
							.finger_id = static_cast<std::uint32_t>(e.finger),
							.coordinate = EventVec2df{
								e.x,
								e.y,
							},
							.pressure = e.pressure,
						}
					}
				}
			};
		}
		ControllerEvent::SensorType Translate(SDL_SensorType type) noexcept
		{
			switch (type)
			{
			case SDL_SENSOR_UNKNOWN: return ControllerEvent::SensorType::Unknown;
			case SDL_SENSOR_ACCEL: return ControllerEvent::SensorType::Accelerometer;
			case SDL_SENSOR_GYRO: return ControllerEvent::SensorType::Gyroscope;
#if SDL_VERSION_ATLEAST(2,26,5)
			case SDL_SENSOR_ACCEL_L: return ControllerEvent::SensorType::LeftAccelerometer;
			case SDL_SENSOR_GYRO_L: return ControllerEvent::SensorType::LeftGyroscope;
			case SDL_SENSOR_ACCEL_R: return ControllerEvent::SensorType::RightAccelerometer;
			case SDL_SENSOR_GYRO_R: return ControllerEvent::SensorType::RightGyroscope;
#endif
			default: return ControllerEvent::SensorType::Invalid;
			}
		}
		Event TranslateControllerSensorUpdate(SDL_Event const & event) noexcept
		{
			auto const & e = event.csensor;
			return Event{
#if SDL_VERSION_ATLEAST(2,26,5)
				.timestamp = std::chrono::microseconds(e.timestamp_us),
#else
				.timestamp = std::chrono::microseconds(e.timestamp * 1000),
#endif
				.event_data = ControllerEvent{
					.controller_id = static_cast<std::uint32_t>(e.which),
					.event_data = ControllerEvent::SensorUpdate{
						.sensor_type = Translate(static_cast<SDL_SensorType>(e.sensor)),
						.values = {
							e.data[0],
							e.data[1],
							e.data[2],
						},
					}
				}
			};
		}

		Event TranslateFingerDown(SDL_Event const & event) noexcept
		{
			auto const & e = event.tfinger;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = TouchEvent{
						.touch_id = static_cast<std::uint64_t>(e.touchId),
						.event_data = TouchPadState{
							.finger_id = static_cast<std::uint64_t>(e.fingerId),
							.coordinate = {
								e.x,
								e.y, 
							},
							.pressure = e.pressure,
							.touch_state = true,
						}
					}
				}
			};
		}
		Event TranslateFingerUp(SDL_Event const & event) noexcept
		{
			auto const & e = event.tfinger;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = TouchEvent{
						.touch_id = static_cast<std::uint64_t>(e.touchId),
						.event_data = TouchPadState{
							.finger_id = static_cast<std::uint64_t>(e.fingerId),
							.coordinate = {
								e.x,
								e.y,
							},
							.pressure = e.pressure,
							.touch_state = false,
						}
					}
				}
			};
		}
		Event TranslateFingerMotion(SDL_Event const & event) noexcept
		{
			auto const & e = event.tfinger;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = TouchEvent{
						.touch_id = static_cast<std::uint64_t>(e.touchId),
						.event_data = TouchPadMotion{
							.finger_id = static_cast<std::uint64_t>(e.fingerId),
							.coordinate = {
								e.x,
								e.y,
							},
							.pressure = e.pressure
						}
					}
				}
			};
		}
		Event TranslateDollarGesture(SDL_Event const & event) noexcept
		{
			auto const & e = event.dgesture;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TouchEvent{
					.touch_id = static_cast<std::uint64_t>(e.touchId),
					.event_data = TouchEvent::DollarGestureEvent{
						.gesture_id = static_cast<std::uint64_t>(e.gestureId),
						.event_data = TouchEvent::DollarGesture{
							.number_of_fingers = e.numFingers,
							.coordinate = {
								e.x,
								e.y,
							},
							.error = e.error,
						}
					}
				}
			};
		}
		Event TranslateDollarRecord(SDL_Event const & event) noexcept
		{
			auto const & e = event.dgesture;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TouchEvent{
					.touch_id = static_cast<std::uint64_t>(e.touchId),
					.event_data = TouchEvent::DollarGestureEvent{
						.gesture_id = static_cast<std::uint64_t>(e.gestureId),
						.event_data = TouchEvent::DollarGestureRecord{}
					}
				}
			};
		}
		Event TranslateDropFile(SDL_Event const & event) noexcept
		{
			auto const & e = event.drop;
			FreeOnExit auto_free(e.file);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = DropEvent{
						.event_data = DropEvent::File{
							.path = std::filesystem::path(e.file ? std::string(e.file) : ""),
						}
					}
				}
			};
		}
		Event TranslateDropText(SDL_Event const & event) noexcept
		{
			auto const & e = event.drop;
			FreeOnExit auto_free(e.file);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = DropEvent{
						.event_data = DropEvent::Text{
							.text = ToUtf8String(e.file),
						}
					}
				}
			};
		}
		Event TranslateDropBegin(SDL_Event const & event) noexcept
		{
			auto const & e = event.drop;
			FreeOnExit auto_free(e.file);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = DropEvent{
						.event_data = DropEvent::Begin{},
					}
				}
			};
		}
		Event TranslateDropComplete(SDL_Event const & event) noexcept
		{
			auto const & e = event.drop;
			FreeOnExit auto_free(e.file);
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = WindowEvent{
					.window_id = e.windowID,
					.event_data = DropEvent{
						.event_data = DropEvent::Complete{},
					}
				}
			};
		}
		Event TranslateMultiGesture(SDL_Event const & event) noexcept
		{
			auto const & e = event.mgesture;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = TouchEvent{
					.touch_id = static_cast<std::uint64_t>(e.touchId),
					.event_data = TouchEvent::MultiGesture{
						.theta = e.dTheta,
						.distance = e.dDist,
						.coordinate = {
							e.x,
							e.y,
						},
						.number_of_fingers = e.numFingers,
					},
				},
			};
		}
		Event TranslateClipboardUpdate(SDL_Event const & event) noexcept
		{
			auto const & e = event.common;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = AppEvent::ClipboardUpdate,
			};
		}
		AudioDeviceEventType TranslateAudioEvent(SDL_AudioDeviceEvent const & e, bool added) noexcept
		{
			return (e.iscapture == 0)
				? ( added // Output
					? AudioDeviceEventType::OutputAdded
					: AudioDeviceEventType::OutputRemoved )
				: ( added // Capture
					? AudioDeviceEventType::CaptureAdded
					: AudioDeviceEventType::CaptureRemoved );
		}
		Event TranslateAudioDeviceAdded(SDL_Event const & event) noexcept
		{
			auto const & e = event.adevice;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = AudioDeviceEvent{
					.audio_device_id = e.which,
					.type = TranslateAudioEvent(e, true)
				},
			};
		}
		Event TranslateAudioDeviceRemoved(SDL_Event const & event) noexcept
		{
			auto const & e = event.adevice;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = AudioDeviceEvent{
					.audio_device_id = e.which,
					.type = TranslateAudioEvent(e, false)
				}
			};
		}
		Event TranslateSensorUpdate(SDL_Event const & event) noexcept
		{
			auto const & e = event.sensor;
			return Event{
#if SDL_VERSION_ATLEAST(2,26,5)
				.timestamp = std::chrono::microseconds(e.timestamp_us),
#else
				.timestamp = std::chrono::milliseconds(e.timestamp),
#endif
				.event_data = SensorEvent{
					.instance_id = static_cast<std::uint32_t>(e.which),
					.data = { e.data[0], e.data[1], e.data[2], e.data[3], e.data[4], e.data[5] },
				},
			};
		}
		Event TranslateRenderTargetsReset(SDL_Event const & event) noexcept
		{
			auto const & e = event.common;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = AppEvent::RenderTargetsReset,
			};
		}
		Event TranslateRenderDeviceReset(SDL_Event const & event) noexcept
		{
			auto const & e = event.common;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = AppEvent::RenderDeviceReset,
			};
		}
		Event TranslateUserEvent(SDL_Event const & event) noexcept
		{
			auto const & e = event.user;
			return Event{
				.timestamp{ std::chrono::milliseconds(e.timestamp) },
				.event_data = UserEvent{},
			};
		}

		Event ConvertToEvent(SDL_Event const & event) noexcept
		{
			switch (event.type)
			{
			case SDL_QUIT:						return TranslateQuit(event);
			case SDL_APP_TERMINATING:			return TranslateAppTerminating(event);
			case SDL_FIRSTEVENT: 				return TranslateFirstEvent(event);
			case SDL_APP_LOWMEMORY: 			return TranslateAppLowMemory(event);
			case SDL_APP_WILLENTERBACKGROUND: 	return TranslateAppWillEnterBackground(event);
			case SDL_APP_DIDENTERBACKGROUND: 	return TranslateAppDidEnterBackground(event);
			case SDL_APP_WILLENTERFOREGROUND: 	return TranslateAppWillEnterForeground(event);
			case SDL_APP_DIDENTERFOREGROUND: 	return TranslateAppDidEnterForeground(event);
			case SDL_LOCALECHANGED: 			return TranslateLocaleChanged(event);
			case SDL_DISPLAYEVENT: 				return TranslateDisplayEvent(event);
			case SDL_WINDOWEVENT: 				return TranslateWindowEvent(event);
			case SDL_KEYDOWN: 					return TranslateKeyDown(event);
			case SDL_KEYUP: 					return TranslateKeyUp(event);
			case SDL_TEXTEDITING: 				return TranslateTextEditing(event);
			case SDL_TEXTINPUT: 				return TranslateTextInput(event);
			case SDL_TEXTEDITING_EXT: 			return TranslateTexteditingExt(event);
			case SDL_KEYMAPCHANGED: 			return TranslateKeymapChanged(event);
			case SDL_MOUSEMOTION:				return TranslateMouseMotion(event);
			case SDL_MOUSEBUTTONDOWN: 			return TranslateMouseButtonDown(event);
			case SDL_MOUSEBUTTONUP: 			return TranslateMouseButtonUp(event);
			case SDL_MOUSEWHEEL: 				return TranslateMouseWheel(event);
			case SDL_JOYAXISMOTION: 			return TranslateJoyaxisMotion(event);
			case SDL_JOYBALLMOTION: 			return TranslateJoyballMotion(event);
			case SDL_JOYHATMOTION: 				return TranslateJoyhatMotion(event);
			case SDL_JOYBUTTONDOWN: 			return TranslateJoyButtonDown(event);
			case SDL_JOYBUTTONUP: 				return TranslateJoyButtonUp(event);
			case SDL_JOYDEVICEADDED: 			return TranslateJoyDeviceAdded(event);
			case SDL_JOYDEVICEREMOVED: 			return TranslateJoyDeviceRemoved(event);
			case SDL_JOYBATTERYUPDATED: 		return TranslateJoyBatteryUpdated(event);
			case SDL_CONTROLLERBUTTONUP: 		return TranslateControllerButtonUp(event);
			case SDL_CONTROLLERBUTTONDOWN: 		return TranslateControllerButtonDown(event);
			case SDL_CONTROLLERTOUCHPADUP: 		return TranslateControllerTouchPadUp(event);
			case SDL_CONTROLLERTOUCHPADDOWN: 	return TranslateControllerTouchPadDown(event);
			case SDL_CONTROLLERDEVICEADDED: 	return TranslateControllerDeviceAdded(event);
			case SDL_CONTROLLERDEVICEREMOVED: 	return TranslateControllerDeviceRemoved(event);
			case SDL_CONTROLLERDEVICEREMAPPED: 	return TranslateControllerDeviceRemapped(event);
			case SDL_CONTROLLERAXISMOTION: 		return TranslateControllerAxisMotion(event);
			case SDL_CONTROLLERTOUCHPADMOTION: 	return TranslateControllerTouchPadMotion(event);
			case SDL_CONTROLLERSENSORUPDATE: 	return TranslateControllerSensorUpdate(event);
			case SDL_FINGERDOWN: 				return TranslateFingerDown(event);
			case SDL_FINGERUP: 					return TranslateFingerUp(event);
			case SDL_FINGERMOTION: 				return TranslateFingerMotion(event);
			case SDL_DOLLARGESTURE: 			return TranslateDollarGesture(event);
			case SDL_DOLLARRECORD: 				return TranslateDollarRecord(event);
			case SDL_MULTIGESTURE: 				return TranslateMultiGesture(event);
			case SDL_CLIPBOARDUPDATE: 			return TranslateClipboardUpdate(event);
			case SDL_DROPFILE: 					return TranslateDropFile(event);
			case SDL_DROPTEXT: 					return TranslateDropText(event);
			case SDL_DROPBEGIN: 				return TranslateDropBegin(event);
			case SDL_DROPCOMPLETE: 				return TranslateDropComplete(event);
			case SDL_AUDIODEVICEADDED: 			return TranslateAudioDeviceAdded(event);
			case SDL_AUDIODEVICEREMOVED: 		return TranslateAudioDeviceRemoved(event);
			case SDL_SENSORUPDATE: 				return TranslateSensorUpdate(event);
			case SDL_RENDER_TARGETS_RESET: 		return TranslateRenderTargetsReset(event);
			case SDL_RENDER_DEVICE_RESET: 		return TranslateRenderDeviceReset(event);
			case SDL_USEREVENT: 				return TranslateUserEvent(event);
			default: 							return Event{ .timestamp{ std::chrono::milliseconds(std::chrono::milliseconds(event.common.timestamp)) }, .event_data = AppEvent::Error };
			}
		}

		std::vector<Event> ConvertToEvents(std::vector<SDL_Event> const & events) noexcept
		{
			std::vector<Event> output(std::size(events));
			std::transform(std::cbegin(events), std::cend(events), std::begin(output), ConvertToEvent);
			return output;
		}
	}

	void PumpEvents() noexcept
	{
		SDL_PumpEvents();
	}

	auto PollEvents(std::size_t maximum_events_per_batch) noexcept -> std::vector<Event>
	{
		auto events = std::vector<SDL_Event>(maximum_events_per_batch);
		auto const event_count = SDL_PeepEvents(events.data(), static_cast<int>(events.size()), SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT);
		if (event_count <= 0) return {};
		events.resize(event_count);
		return ConvertToEvents(events);
	}
}
#endif