#pragma once
#include "cpos/OpCpoFund.hpp"

namespace Cst
{
	template <auto Tag>
	using Value = OpCpo::Fund<double, Tag>;

	template <auto Tag>
	struct Side
	{
		Value<Tag> from{ 0.0 };
		Value<Tag> to{ 0.0 };
	};

	enum class Types { X, Y };

	using XValue = Value<Types::X>;
	using YValue = Value<Types::Y>;

	struct Pt
	{
		XValue x{ 0.0 };
		YValue y{ 0.0 };
	};

	struct Size
	{
		XValue width{ 0.0 };
		YValue height{ 0.0 };
	};

	using XSide = Side<Types::X>;
	using YSide = Side<Types::Y>;

	template <typename T>
	struct XSplit
	{
		T left;
		T right;
	};

	template <typename T>
	struct YSplit
	{
		T bottom;
		T top;
	};

	struct Rect
	{
		Pt pt;
		Size rect;

		[[nodiscard]] XSplit<Rect> SplitOn(XValue value) const noexcept;
		[[nodiscard]] YSplit<Rect> SplitOn(YValue value) const noexcept;

		[[nodiscard]] Rect ExtendFromLeft(XValue value) const noexcept;
		[[nodiscard]] Rect ExtendFromRight(XValue value) const noexcept;
		[[nodiscard]] Rect ExtendFromTop(YValue value) const noexcept;
		[[nodiscard]] Rect ExtendFromBottom(YValue value) const noexcept;

		[[nodiscard]] Rect Inset(XValue x_amount, YValue y_amount) const noexcept;
		[[nodiscard]] Rect Outset(XValue x_amount, YValue y_amount) const noexcept;
		[[nodiscard]] Rect Offset(XValue left, XValue right, YValue top, YValue bottom) const noexcept;
	};
}