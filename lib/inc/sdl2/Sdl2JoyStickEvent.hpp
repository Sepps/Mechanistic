#pragma once
#include "Sdl2EventCommon.hpp"
#include "sdl2/Sdl2Button.hpp"
#include <variant>

namespace Jam
{
	struct JoyStickEvent
	{
		struct JoyaxisMotion
		{
			std::uint32_t axis = 0;
			std::int16_t value = 0;
		};
		struct JoyballMotion
		{
			std::uint32_t ball_id = 0;
			EventVec2d relative_motion;
		};
		enum class JoyhatMotionPosition
		{
			Centered,
			Down,
			Left,
			LeftDown,
			LeftUp,
			Right,
			RightDown,
			RightUp,
			Up,
		};
		struct JoyhatMotion
		{
			std::uint32_t hat_id = 0;
			JoyhatMotionPosition motion_position = JoyhatMotionPosition::Left;
		};
		struct JoyDeviceAdded{};
		struct JoyDeviceRemoved{};
		enum class JoyStickPowerLevel
		{
			Unknown,
			Empty,
			Low,
			Medium,
			Full,
			Wired,
			Max,
		};

		std::uint32_t joystick_id = 0;
		std::variant<
			JoyaxisMotion,
			JoyballMotion,
			JoyhatMotionPosition,
			JoyhatMotion,
			JoyDeviceAdded,
			JoyDeviceRemoved,
			JoyStickPowerLevel,
			Button
		> event_data;
	};
}