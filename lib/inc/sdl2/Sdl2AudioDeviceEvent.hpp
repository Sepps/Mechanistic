#pragma once
#include <cstdint>

namespace Jam
{
	enum class AudioDeviceEventType
	{
		CaptureAdded,
		CaptureRemoved,
		OutputAdded,
		OutputRemoved
	};
	struct AudioDeviceEvent
	{
		std::uint32_t audio_device_id = 0;
		AudioDeviceEventType type = AudioDeviceEventType::CaptureAdded;
	};
}