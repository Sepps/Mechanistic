#pragma once
#include "Sdl2EventCommon.hpp"
#include "Sdl2TouchPadState.hpp"
#include "Sdl2Button.hpp"
#include <cstdint>
#include <variant>
#include <chrono>

namespace Jam
{
	struct ControllerEvent
	{
		enum class Device
		{
			Added,
			Removed,
			Remapped,
		};
		enum class SensorType
		{
			Invalid,
			Unknown,
			Accelerometer,
			Gyroscope,
			LeftAccelerometer,
			RightAccelerometer,
			LeftGyroscope,
			RightGyroscope,
		};
		struct AxisMotion
		{
			std::uint32_t axis = 0;
			std::int16_t value = 0;
		};
		struct SensorUpdate
		{
			SensorType sensor_type;
			EventVec3df values{};
		};
		struct TouchState
		{
			std::uint32_t touch_id = 0;
			TouchPadState touch_pad_state;
		};
		struct TouchMotion
		{
			std::uint32_t touch_id = 0;
			TouchPadMotion touch_pad_motion;
		};

		std::uint32_t controller_id = 0;
		std::variant<AxisMotion, Button, TouchState, TouchMotion, Device, SensorUpdate> event_data;
	};
}