#pragma once
#include <cstdint>

namespace Jam
{
	enum class DisplayEventType
	{
		Orientation,
		Connected,
		Disconnected,
	};

	struct DisplayEvent
	{
		std::uint32_t display_index = 0;
		DisplayEventType event_type = {};
	};
}