#pragma once
#include "Sdl2KeyCodes.hpp"

namespace Jam
{
	struct KeySides
	{
		bool left = false;
		bool right = false;
		[[nodiscard]] constexpr operator bool() const noexcept { return (left or right); }
	};
	struct KeyStateModifiers
	{
		KeySides shift;
		KeySides ctrl;
		KeySides alt;
		KeySides os;
	};
	struct KeyToggleModifiers
	{
		bool num_lock = false;
		bool caps_lock = false;
		bool scroll_lock = false;
	};
	struct KeyModifiers
	{
		KeyStateModifiers state = {};
		KeyToggleModifiers toggle = {};
	};
	struct KeyEvent
	{
		bool key_state = false;
		bool is_repeat = false;
		KeyModifiers modifiers = {};
		KeyCode key_code = KeyCode::Unknown;
		KeyCode scan_code = KeyCode::Unknown;
	};
}