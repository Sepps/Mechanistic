#pragma once

namespace Jam
{
	// defined as an event that requires application action.
	// for example, low memory should cause the app to release some memory.
	enum class AppEvent
	{
		AppDidEnterBackground,
		AppDidEnterForeground,
		AppWillEnterBackground,
		AppWillEnterForeground,
		LowMemory,
		Quit,
		Error,
		RenderDeviceReset,
		RenderTargetsReset,
		Terminating,
		LocaleChanged,
		KeymapChanged,
		ClipboardUpdate,
	};
}