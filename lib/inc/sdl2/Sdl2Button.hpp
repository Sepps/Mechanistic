#pragma once
#include <cstdint>

namespace Jam
{
	struct Button
	{
		std::uint32_t id = 0;    // the id of the particular button, this is unique for a particular peripheral.
		std::uint32_t count = 1; // number of times button was pressed.
		bool state = 0;          // is the button pressed (true) or released (false).
	};
}