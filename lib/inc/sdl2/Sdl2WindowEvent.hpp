#pragma once
#include "Sdl2KeyEvent.hpp"
#include "Sdl2TextEvent.hpp"
#include "Sdl2MouseEvent.hpp"
#include "Sdl2TouchEvent.hpp"
#include "Sdl2DropEvent.hpp"
#include <variant>
#include <cstdint>

namespace Jam
{
	struct WindowEvent
	{
		struct Shown{};
		struct Hidden{};
		struct Exposed{};
		struct Moved{};
		struct Resized{};
		struct SizeChanged{};
		struct Minimized{};
		struct Maximized{};
		struct Restored{};
		struct Enter{};
		struct Leave{};
		struct FocusGained{};
		struct FocusLost{};
		struct Close{};
		struct TakeFocus{};
		struct HitTest{};
		struct IccprofChanged{};
		struct DisplayChanged{};

		using event_data_type = std::variant<
			Shown,
			Hidden,
			Exposed,
			Moved,
			Resized,
			SizeChanged,
			Minimized,
			Maximized,
			Restored,
			Enter,
			Leave,
			FocusGained,
			FocusLost,
			Close,
			TakeFocus,
			HitTest,
			IccprofChanged,
			DisplayChanged,
			KeyEvent,
			MouseEvent,
			TextEvent,
			TouchEvent,
			DropEvent
		>;

		std::uint32_t window_id = 0;
		event_data_type event_data;
	};
}