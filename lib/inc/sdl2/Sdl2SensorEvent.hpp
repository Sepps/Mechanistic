#pragma once
#include <chrono>
#include <array>

namespace Jam
{
	struct SensorEvent
	{
		std::uint32_t instance_id = 0;
		std::array<float, 6> data{};
	};
}