#pragma once
#include "Sdl2AppEvent.hpp"
#include "Sdl2UserEvent.hpp"
#include "Sdl2ControllerEvent.hpp"
#include "Sdl2JoyStickEvent.hpp"
#include "Sdl2SensorEvent.hpp"
#include "Sdl2DisplayEvent.hpp"
#include "Sdl2KeyEvent.hpp"
#include "Sdl2AudioDeviceEvent.hpp"
#include "Sdl2TouchEvent.hpp"
#include "Sdl2WindowEvent.hpp"

#include <chrono>
#include <variant>
#include <cstddef>
#include <span>
#include <vector>

namespace Jam
{
	struct Event
	{
		std::chrono::nanoseconds timestamp;
		std::variant<
			AppEvent,
			UserEvent,
			ControllerEvent,
			JoyStickEvent,
			SensorEvent,
			DisplayEvent,
			KeyEvent,
			AudioDeviceEvent,
			TouchEvent,
			WindowEvent
		> event_data;
	};
	/**
	 * @brief Pump the events to the poll function.
	 * 
	 * @warning Must be called from the thread with the video subsystem.
	 */
	void PumpEvents() noexcept;
	/**
	 * @brief Poll the current pending events.
	 * 
	 * @param maximum_events_per_batch The maximum number of events to receive at once.
	 * @return A vector full of events.
	 */
	[[nodiscard]] auto PollEvents(std::size_t maximum_events_per_batch = 8) noexcept -> std::vector<Event>;
}