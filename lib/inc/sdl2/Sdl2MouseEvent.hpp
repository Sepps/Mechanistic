#pragma once
#include "Sdl2EventCommon.hpp"
#include "Sdl2Button.hpp"
#include <variant>
#include <cstdint>

namespace Jam
{
	enum struct MouseButton : std::uint32_t
	{
		Left,
		Middle,
		Right,
		X1,
		X2,
	};

	template <MouseButton>
	struct MouseButtonEvent : Button {};

	struct MouseEvent
	{
		struct Motion : EventVec2df {};
		struct Wheel : EventVec2df {};
		std::uint32_t mouse_instance_id = 0;
		EventVec2d coordinate;
		std::variant
		<
			Motion,
			Wheel,
			MouseButtonEvent<MouseButton::Left>,
			MouseButtonEvent<MouseButton::Middle>,
			MouseButtonEvent<MouseButton::Right>,
			MouseButtonEvent<MouseButton::X1>,
			MouseButtonEvent<MouseButton::X2>
		> event_data;
	};
}