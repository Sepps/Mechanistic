#pragma once
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Jam
{
	using EventVec2d  = glm::vec<2, std::int32_t>;
	using EventVec2df = glm::vec<2, float>;
	using EventVec3d  = glm::vec<3, std::int32_t>;
	using EventVec3df = glm::vec<3, float>;
}