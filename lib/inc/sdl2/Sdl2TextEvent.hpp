#pragma once
#include <cstddef>
#include <string>
#include <variant>

namespace Jam
{
	struct TextEvent
	{
		struct Editing
		{
			std::size_t start = 0;
			std::size_t length = 0;
		};
		struct Input{};

		std::u8string text;
		std::variant<Editing, Input> event_data;
	};
}