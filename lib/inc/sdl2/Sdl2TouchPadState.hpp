#pragma once
#include "Sdl2EventCommon.hpp"
#include <cstdint>

namespace Jam
{
	struct TouchPadState
	{
		std::uint64_t finger_id = 0;
		EventVec2df coordinate;
		float pressure = 0;
		bool touch_state = false;
	};
	struct TouchPadMotion
	{
		std::uint64_t finger_id = 0;
		EventVec2df coordinate;
		float pressure = 0;
	};
}