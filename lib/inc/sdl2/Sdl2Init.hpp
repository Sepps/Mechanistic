#pragma once
#include <initializer_list>
#include <array>
#include <cstdint>

namespace Jam
{
	enum class Sdl2Subsystem
	{
		Timer = 0,
		Audio = 1,
		Video = 2,
		Joystick = 3,
		NoParachute = 4,
	};

	inline constexpr std::array sdl2_avaliable_subsystems
	{
		Sdl2Subsystem::Timer,
		Sdl2Subsystem::Audio,
		Sdl2Subsystem::Video,
		Sdl2Subsystem::Joystick,
		Sdl2Subsystem::NoParachute
	};
	inline constexpr std::size_t sdl2_subsystem_count = sdl2_avaliable_subsystems.size();

	struct Sdl2Funcs
	{
		using init_func = int (*)(void *, std::uint32_t flags);
		using quit_subsubsystem_func = void(*)(void *, std::uint32_t flags);
		using quit_func = void(*)(void *);

		void * obj_ptr = nullptr;
		init_func init_subsystem = nullptr;
		quit_subsubsystem_func quit_subsystem = nullptr;
		quit_func quit = nullptr;
	};

	[[nodiscard]] auto GetSdl2InitFuncs() noexcept -> Sdl2Funcs const &;

	struct Sdl2Init
	{
		Sdl2Init(std::initializer_list<Sdl2Subsystem> flags, Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept;
		~Sdl2Init() noexcept;

		[[nodiscard]] bool IsTimerEnabled() const noexcept;
		[[nodiscard]] bool IsAudioEnabled() const noexcept;
		[[nodiscard]] bool IsVideoEnabled() const noexcept;
		[[nodiscard]] bool IsJoystickEnabled() const noexcept;
		[[nodiscard]] bool IsNoParachuteEnabled() const noexcept;
		[[nodiscard]] bool UsesSubsystem() const noexcept;
		[[nodiscard]] bool IsInitialized() const noexcept;

		void SetSdl2Funcs(Sdl2Funcs const * const sdl2_funcs) noexcept;
	private:
		Sdl2Funcs const * m_sdl2_funcs = nullptr;
		bool m_initialized = false;
		std::array<bool, sdl2_subsystem_count> m_used_subsystems{};
	};

	struct Sdl2TimerSubsystem : Sdl2Init
	{
		Sdl2TimerSubsystem(Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept : Sdl2Init{ { Sdl2Subsystem::Timer }, sdl2_funcs } {}
	};
	struct Sdl2AudioSubsystem : Sdl2Init
	{
		Sdl2AudioSubsystem(Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept : Sdl2Init{ { Sdl2Subsystem::Audio }, sdl2_funcs } {}
	};
	struct Sdl2VideoSubsystem : Sdl2Init
	{
		Sdl2VideoSubsystem(Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept : Sdl2Init{ { Sdl2Subsystem::Video }, sdl2_funcs } {}
	};
	struct Sdl2JoystickSubsystem : Sdl2Init
	{
		Sdl2JoystickSubsystem(Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept : Sdl2Init{ { Sdl2Subsystem::Joystick }, sdl2_funcs } {}
	};
	struct Sdl2NoParachuteSubsystem : Sdl2Init
	{
		Sdl2NoParachuteSubsystem(Sdl2Funcs const & sdl2_funcs = GetSdl2InitFuncs()) noexcept : Sdl2Init{ { Sdl2Subsystem::NoParachute }, sdl2_funcs } {}
	};
}