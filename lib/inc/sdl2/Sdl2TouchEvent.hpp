#pragma once
#include "Sdl2EventCommon.hpp"
#include "Sdl2TouchPadState.hpp"
#include <variant>

namespace Jam
{
	struct TouchEvent
	{
		struct DollarGesture
		{
			std::uint32_t number_of_fingers = 0;
			EventVec2df coordinate;
			float error = 0;
		};
		struct DollarGestureRecord{};
		struct DollarGestureEvent
		{
			std::uint64_t gesture_id = 0;
			std::variant<DollarGesture, DollarGestureRecord> event_data;
		};
		struct MultiGesture
		{
			float theta = 0;
			float distance = 0;
			EventVec2df coordinate;
			std::uint16_t number_of_fingers = 0;
		};

		std::uint64_t touch_id = 0;
		std::variant<DollarGestureEvent, MultiGesture, TouchPadState, TouchPadMotion> event_data;
	};
} 