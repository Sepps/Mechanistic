#pragma once
#include "WindowBase.hpp"
#include <memory>

namespace Jam
{
	enum class WindowBackend : std::uint32_t
	{
		Default = 0,
		#ifdef SUPPORTS_SDL2
		Sdl2 = 1,
		#endif
	};

	struct Window final
	{
		Window(Window &&);
		Window(Window const &) = delete;
		Window& operator=(Window &&);
		Window& operator=(Window const &) = delete;

		[[nodiscard]] WindowType GetWindowType() const;
		[[nodiscard]] WindowFocus GetWindowFocus() const;

		[[nodiscard]] std::string GetTitle() const;
		bool SetTitle(std::string title);

		[[nodiscard]] WindowExtents GetExtents() const;
		bool SetExtents(WindowExtents extents);

		[[nodiscard]] WindowDisplayState GetWindowDisplayState() const;
		bool SetWindowDisplayState(WindowDisplayState state);

		[[nodiscard]] WindowFlags GetWindowFlags() const;
		bool SetWindowFlags(WindowFlags flags);

		[[nodiscard]] WindowGraphics GetWindowGraphics() const;
		bool SetWindowGraphics(WindowGraphics graphics);

		bool FlashWindow(WindowFlashType type);
		bool RaiseWindow();
		bool SetWindowIcon(std::filesystem::path path);

		void SwapBuffers();

		[[nodiscard]] auto GetOpenGlContext() -> WindowBase::opengl_context;
	private:
		friend Window CreateWindow(WindowBackend backend, std::string title, WindowExtents extents, WindowSettings const & settings);
		friend Window CreateDefaultWindow(std::string title, WindowExtents extents, WindowSettings const & settings);
		Window(WindowBase * ptr) noexcept;
		std::unique_ptr<WindowBase> m_impl = nullptr;
	};

	static_assert(not std::is_default_constructible_v<Window>);
	static_assert(not std::is_copy_constructible_v<Window>);
	static_assert(not std::is_copy_assignable_v<Window>);
	static_assert(std::is_move_constructible_v<Window>);
	static_assert(std::is_move_assignable_v<Window>);

	[[nodiscard]] Window CreateWindow(WindowBackend backend, std::string title, WindowExtents extents, WindowSettings const & settings = {});
	[[nodiscard]] Window CreateDefaultWindow(std::string title, WindowExtents extents, WindowSettings const & settings = {});
}