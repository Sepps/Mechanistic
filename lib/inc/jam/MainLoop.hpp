#pragma once
#include "FnRef/FnRef.h"

namespace Jam
{
	void MainLoop(Hz::Fn::FnRef<void() noexcept> callback) noexcept;
	void OtherLoop(Hz::Fn::FnRef<void() noexcept> callback) noexcept;
}