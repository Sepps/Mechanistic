#pragma once
#include <cstdint>
#include <string>
#include <filesystem>

namespace Jam
{
	struct WindowFocus
	{
		bool input_focus;
		bool mouse_focus;
	};

	enum class WindowType : std::uint32_t
	{
		Normal,
		Tooltip,
		Utility,
		PopupMenu,
	};

	struct WindowFlags
	{
		bool hidden = false;
		bool fullscreen = false;
		bool always_on_top = false;
		bool borderless = false;
		bool resizable = true;
		bool mouse_capture = false;
		bool skip_taskbar = false;
		bool mouse_grabbed = false;
		bool keyboard_grabbed = false;
	};

	enum class WindowDisplayState : std::uint32_t
	{
		Restored,
		Minimised,
		Maximised,
	};

	struct WindowGraphics
	{
		bool opengl = false;
		bool vulkan = false;
	};

	struct WindowSettings
	{
		WindowType type = WindowType::Normal;
		WindowFlags flags = {};
		WindowDisplayState display_state = WindowDisplayState::Restored;
		WindowGraphics graphics = {};
	};

	struct Centered{};

	struct WindowPosition
	{
		bool centered = true;
		std::uint32_t value = 0;

		constexpr WindowPosition(auto v)
			: centered(IsCentered(v))
			, value(Value(v))
		{}
	private:
		static constexpr std::uint32_t Value(std::uint32_t const & v) noexcept { return v; }
		static constexpr std::uint32_t Value(Centered const & v) noexcept { return 0; }
		static constexpr auto IsCentered(std::uint32_t const & v) noexcept { return false; }
		static constexpr auto IsCentered(Centered const & v) noexcept { return true; }
	};

	struct WindowExtents
	{
		WindowPosition x = { Centered{} };
		WindowPosition y = { Centered{} };
		std::uint32_t width = 360;
		std::uint32_t height = 240;
	};

	enum class WindowFlashType : std::uint32_t
	{
		Cancel,
		Briefly,
		UntilFocused,
	};

	struct WindowBase
	{
		inline virtual ~WindowBase() {};

		[[nodiscard]] virtual WindowType GetWindowType() const = 0;
		[[nodiscard]] virtual WindowFocus GetWindowFocus() const = 0;

		[[nodiscard]] virtual std::string GetTitle() const = 0;
		virtual bool SetTitle(std::string title) = 0;

		[[nodiscard]] virtual WindowExtents GetExtents() const = 0;
		virtual bool SetExtents(WindowExtents extents) = 0;

		[[nodiscard]] virtual WindowDisplayState GetWindowDisplayState() const = 0;
		virtual bool SetWindowDisplayState(WindowDisplayState state) = 0;

		[[nodiscard]] virtual WindowFlags GetWindowFlags() const = 0;
		virtual bool SetWindowFlags(WindowFlags flags) = 0;

		[[nodiscard]] virtual WindowGraphics GetWindowGraphics() const = 0;
		virtual bool SetWindowGraphics(WindowGraphics graphics) = 0;

		virtual bool FlashWindow(WindowFlashType type) = 0;
		virtual bool RaiseWindow() = 0;
		virtual bool SetWindowIcon(std::filesystem::path path) = 0;
		virtual void SwapBuffers() = 0;

		using opengl_context = std::unique_ptr<void, void(*)(void*)>;
		[[nodiscard]] virtual auto GetOpenGlContext() -> opengl_context = 0;
	};
}