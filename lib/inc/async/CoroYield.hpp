#pragma once
#include "CoroScheduable.hpp"
#include "async/Scheduler.hpp"

namespace Jam
{
	template <typename Exe = Jam::Scheduler>
	class Yield
	{
		struct Resumer
		{
			void * ptr = nullptr;
			void (*fn)(void*);
			void operator()() const noexcept { fn(ptr); }
		};
		Resumer resumer;
	public:
		static constexpr bool await_ready() noexcept
		{
			return false;
		}
		template <CanGetScheduler<Exe> Promise>
		void await_suspend(std::coroutine_handle<Promise> handle) noexcept
		{
			resumer.ptr = handle.address();
			resumer.fn = +[](void * ptr) noexcept { std::coroutine_handle<Promise>::from_address(ptr).resume(); };
			handle.promise().get_scheduler().Push(resumer);
		}
		static void await_resume() noexcept {}
	};
}