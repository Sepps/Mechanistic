#pragma once
#include "CoroContinueWith.hpp"
#include "CoroPromiseValue.hpp"
#include "CoroScheduable.hpp"
#include "Scheduler.hpp"


namespace Jam
{
	template <typename V>
	struct PromiseReturn : PromiseValue<V>
	{
		void return_value( std::constructible_from<V> auto && value ) noexcept
		{
			this->set_value( std::forward<decltype( value )>( value ) );
		}
	};
	template <>
	struct PromiseReturn<void>
	{
		static void return_void() noexcept {}
		static void extract_value() noexcept {}
	};

	template <typename T, typename Exe = Jam::Scheduler>
	struct Task
	{
		struct promise_type : PromiseReturn<T>, PromiseSchedulerSourceSink<Exe>, ContinueWithPromise
		{
			ContinueWithAwaitable final_suspend() noexcept
			{
				return { get_continuation() };
			}
			[[nodiscard]] Task get_return_object() noexcept
			{
				return Task{ handle_type::from_promise( *this ) };
			}
		};
		using handle_type = std::coroutine_handle<promise_type>;
		[[nodiscard]] auto operator co_await() noexcept -> Coawaitable<Exe, promise_type>
		{
			return { handle };
		}
		[[nodiscard]] auto extract_value() const noexcept -> T
		{
			return handle.promise().extract_value();
		}

		Task() = delete;
		explicit Task( Task && task ) noexcept
			: handle{ std::exchange( task.handle, {} ) }
		{
		}
		Task & operator=( Task && task ) noexcept
		{
			handle = std::exchange( task.handle, {} );
			return *this;
		}
		~Task()
		{
			if ( handle ) handle.destroy();
		}
		Task( Task const & ) = delete;
		Task & operator=( Task const & ) = delete;

	private:
		explicit Task( handle_type h ) noexcept
			: handle( h )
		{
		}

		handle_type handle;
	};
}