#pragma once
#include "CoroCoawaitable.hpp"
#include "CoroContinueWith.hpp"
#include "CoroPromiseValue.hpp"
#include "CoroScheduable.hpp"
#include "FnRef/FnRef.h"

#include <coroutine>
#include <iterator>
#include <type_traits>

namespace Jam
{
	template <typename T>
	concept Yieldable = not std::is_void_v<T>;

	struct GenSentinal
	{
	};

	template <typename Exe, typename Promise>
	struct GenIterator
	{
		using value_type = Coawaitable<Exe, Promise>;
		using reference = value_type;
		using difference_type = std::make_signed_t<std::size_t>;
		using iterator_tag = std::input_iterator_tag;
		std::coroutine_handle<Promise> handle = {};

		GenIterator & operator++() noexcept
		{
			return *this;
		}
		[[nodiscard]] reference operator*() const noexcept
		{
			return reference{ handle };
		}

		[[nodiscard]] friend bool operator==( GenSentinal, GenIterator const & iter ) noexcept
		{
			return iter.handle.done();
		}
		[[nodiscard]] friend bool operator==( GenIterator const & iter, GenSentinal ) noexcept
		{
			return iter.handle.done();
		}
		[[nodiscard]] friend bool operator!=( GenIterator const & iter, GenSentinal rhs ) noexcept
		{
			return not( iter == rhs );
		}
		[[nodiscard]] friend bool operator!=( GenSentinal lhs, GenIterator const & iter ) noexcept
		{
			return not( lhs == iter );
		}
	};

	template <Yieldable T, typename Exe = Jam::Scheduler>
	struct Gen
	{
		struct promise_type : PromiseValue<T>, PromiseSchedulerSourceSink<Exe>, ContinueWithPromise
		{
			ContinueWithAwaitable final_suspend() noexcept
			{
				return { get_continuation() };
			}

			struct yield_value_awaitable
			{
				static constexpr bool await_ready() noexcept
				{
					return false;
				}
				void await_suspend( std::coroutine_handle<promise_type> caller ) const noexcept
				{
					auto callback = typename Exe::value_type{ Hz::Fn::non_type<&promise_type::resume_caller>, caller.promise() };
					caller.promise().get_scheduler().Push( callback );
				}
				static constexpr void await_resume() {}
			};

			[[nodiscard]] yield_value_awaitable yield_value( std::convertible_to<T> auto && value ) noexcept
			{
				this->set_value( std::forward<decltype( value )>( value ) );
				return {};
			}

			void resume_caller() noexcept
			{
				get_continuation().resume();
			}

			static void return_void() noexcept {}

			[[nodiscard]] Gen get_return_object() noexcept
			{
				return Gen{ handle_type::from_promise( *this ) };
			}
		};
		using handle_type = std::coroutine_handle<promise_type>;
		[[nodiscard]] auto operator co_await() noexcept -> Coawaitable<Exe, promise_type>
		{
			return { handle };
		}
		[[nodiscard]] auto extract_value() const noexcept -> T
		{
			return handle.promise().extract_value();
		}

		Gen() = delete;
		explicit Gen( Gen && task ) noexcept
			: handle{ std::exchange( task.handle, {} ) }
		{
		}
		Gen & operator=( Gen && task ) noexcept
		{
			handle = std::exchange( task.handle, {} );
			return *this;
		}
		Gen( Gen const & ) = delete;
		Gen & operator=( Gen const & ) = delete;
		~Gen()
		{
			if ( handle ) handle.destroy();
		}

		[[nodiscard]] GenIterator<Exe, promise_type> begin() noexcept
		{
			return { handle };
		}
		[[nodiscard]] GenSentinal end() const noexcept
		{
			return {};
		}

	private:
		explicit Gen( handle_type h ) noexcept
			: handle( h )
		{
		}

		handle_type handle = {};
	};
}