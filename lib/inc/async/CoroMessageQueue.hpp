#pragma once
#include "FnRef/FnRef.h"
#include "async/CoroScheduable.hpp"
#include "async/Scheduler.hpp"

#include <deque>
#include <optional>
#include <type_traits>


namespace Jam
{
	template <typename T, typename Exe = Jam::Scheduler, typename Alloc = std::allocator<T>>
	requires( std::is_default_constructible_v<T> ) struct MessageQueue
	{
		void Push( T const & value )
		{
			auto opt_output = PopWaiter();
			if ( opt_output )
			{
				auto & output = *opt_output;
				output->data = value;
				output->scheduler_ptr->Push( output->handle );
			}
			else { m_queue.push_back( value ); }
		}

		[[nodiscard]] std::optional<T> Pop()
		{
			return PopValue();
		}

		struct waiter_awaitable
		{
			MessageQueue * message_queue_ptr = nullptr;
			Scheduler * scheduler_ptr = nullptr;
			std::optional<T> data;
			std::coroutine_handle<> handle;

			[[nodiscard]] bool await_ready() noexcept
			{
				data = message_queue_ptr->Pop();
				return data.has_value();
			}

			template <CanGetScheduler<Exe> Promise>
			void await_suspend( std::coroutine_handle<Promise> handle ) noexcept
			{
				this->scheduler_ptr = std::addressof( handle.promise().get_scheduler() );
				this->handle = handle;
				message_queue_ptr->m_waiters.push_back( this );
			}

			[[nodiscard]] std::optional<T> await_resume() const noexcept
			{
				return data.has_value() ? *data : message_queue_ptr->PopValue();
			}
		};

		[[nodiscard]] waiter_awaitable AsyncPop() noexcept
		{
			return { this };
		}

	private:
		std::optional<waiter_awaitable *> PopWaiter()
		{
			if ( m_waiters.empty() ) return {};
			auto out = m_waiters.front();
			m_waiters.pop_front();
			return { std::move( out ) };
		}
		std::optional<T> PopValue()
		{
			if ( m_queue.empty() ) return {};
			auto out = m_queue.front();
			m_queue.pop_front();
			return { std::move( out ) };
		}
		// Wrap this, since I dunno if I'll keep this dependancy, its heavy handed when I might be able to
		// do it with an atomic pointer swap.
		std::deque<waiter_awaitable *> m_waiters;
		std::deque<T> m_queue;
	};
}