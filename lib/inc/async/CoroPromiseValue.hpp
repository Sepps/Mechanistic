#pragma once
#include "ClearOnDtor.hpp"
#include <memory>
#include <type_traits>
#include <cassert>

namespace Jam
{
	template <typename V>
	struct PromiseValue
	{
		void set_value( V && value ) requires( std::is_move_constructible_v<V> )
		{
			destroy_value();
			new ( std::addressof( data.ret_value ) ) V( std::move( value ) );
			has_value = true;
		}
		void set_value( V const & value ) requires( std::is_copy_constructible_v<V> )
		{
			destroy_value();
			new ( std::addressof( data.ret_value ) ) V( value );
			has_value = true;
		}

		void destroy_value()
		{
			if ( has_value )
			{
				std::destroy_at( std::addressof( data.ret_value ) );
				has_value = false;
			}
		}

		PromiseValue() = default;
		PromiseValue( PromiseValue && other ) noexcept
		{
			*this = std::move( other );
		}
		PromiseValue & operator=( PromiseValue && other ) noexcept
		{
			if ( other.has_value )
			{
				set_value( std::move( other.ret_value ) );
				other.destroy_value();
			}
			return *this;
		}
		PromiseValue( PromiseValue const & ) noexcept = delete;
		PromiseValue & operator=( PromiseValue const & other ) noexcept = delete;
		~PromiseValue()
		{
			destroy_value();
		}

		[[nodiscard]] V extract_value() noexcept
		{
			assert(has_value);
			ClearOnDtor auto_clear{ has_value };
			return std::move( data.ret_value );
		}
	private:
		bool has_value = false;
		union Data { Data() {} ~Data(){} V ret_value; } data;
	};
}