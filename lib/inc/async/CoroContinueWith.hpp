#pragma once
#include "FnRef/AlwaysInline.h"

#include <coroutine>
#include <exception>
#include <utility>


namespace Jam
{
	struct ContinueWithAwaitable
	{
		std::coroutine_handle<> continue_with;

		static bool await_ready() noexcept
		{
			return false;
		}

		[[nodiscard]] auto await_suspend( [[maybe_unused]] std::coroutine_handle<> ) const noexcept -> std::coroutine_handle<>
		{
			return continue_with;
		}

		static void await_resume() noexcept
		{
		}
	};

	struct ContinueWithPromise
	{
		void set_continuation( std::coroutine_handle<> handle ) noexcept
		{
			m_continue_with = handle;
		}
		[[nodiscard]] auto get_continuation() noexcept -> std::coroutine_handle<>
		{
			return m_continue_with;
		}
		std::suspend_always initial_suspend() noexcept
		{
			return {};
		}
		void unhandled_exception() noexcept
		{
			std::terminate();
		}
	private:
		std::coroutine_handle<> m_continue_with;
	};
}