#pragma once
#include "FnRef/FnRef.h"
#include <deque>
#include <coroutine>
#include <mutex>

namespace Jam
{
	struct Scheduler
	{
		using value_type = Hz::Fn::FnRef<void()>;

		void Push(value_type handle);

		// look, I know this should be a named function, but that'd make other things quite messy I'll figure it out!
		void operator()() noexcept;
	private:
		std::mutex m_active_lock;
		bool m_active_elems = false;
		std::deque<value_type> m_elems[2]{};
	};
}