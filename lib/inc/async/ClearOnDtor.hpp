#pragma once

namespace Jam
{
	struct ClearOnDtor
	{
		bool & value;
		~ClearOnDtor() { value = false; }
	};
}