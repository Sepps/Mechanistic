#pragma once
#include "CoroContinueWith.hpp"
#include "CoroScheduable.hpp"
#include "Scheduler.hpp"

namespace Jam
{
	template <typename Exe = Jam::Scheduler>
	struct Entry
	{
		struct promise_type
			: ContinueWithPromise
			, PromiseSchedulerSource<Exe>
		{
			promise_type(Exe & scheduler, auto&&...) noexcept
				: PromiseSchedulerSource<Exe>(scheduler)
			{}
			promise_type(auto && obj, Exe & scheduler, auto&&...) noexcept
				: PromiseSchedulerSource<Exe>(scheduler)
			{}

			static void return_void() noexcept {}
			static void extract_value() noexcept {}
			std::suspend_always final_suspend() noexcept { return {}; }
			[[nodiscard]] Entry get_return_object() noexcept { return Entry{ handle_type::from_promise( *this ) }; }
		};
		using handle_type = std::coroutine_handle<promise_type>;

		Entry() = delete;
		explicit Entry(Entry && task) noexcept
			: handle{ std::exchange( task.handle, {} ) }
		{}
		Entry & operator=(Entry && task) noexcept
		{
			handle = std::exchange( task.handle, {} );
			return *this;
		}
		~Entry() { if ( handle ) handle.destroy(); }
		Entry( Entry const & ) = delete;
		Entry & operator=( Entry const & ) = delete;
		/**
		 * @brief A function that will resume the coroutine, schedulable as an ordinary callable.
		 * 
		 * @warning If this task is invalid, this function does nothing.
		 * @warning If this coroutine is done, this function does nothing.
		 * return true: when coro was resumed.
		 */
		void operator()() noexcept
		{
			if ( not handle ) return;
			if ( handle.done() ) return;
			handle.resume();
		}
	private:
		explicit Entry( handle_type h ) noexcept
			: handle( h )
		{}

		handle_type handle;
	};
}