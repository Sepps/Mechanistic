#pragma once
#include "Scheduler.hpp"

#include <cassert>
#include <concepts>
#include <memory>

namespace Jam
{
	template <typename Exe = Jam::Scheduler>
	struct PromiseSchedulerSource
	{
		[[nodiscard]] Exe & get_scheduler() noexcept
		{
			assert( m_scheduler_ptr != nullptr );
			return *m_scheduler_ptr;
		}
		PromiseSchedulerSource( Exe & scheduler ) noexcept
		{
			m_scheduler_ptr = std::addressof( scheduler );
		}

	private:
		Exe * m_scheduler_ptr = nullptr;
	};
	template <typename Exe = Jam::Scheduler>
	struct PromiseSchedulerSourceSink
	{
		void set_scheduler( Exe & scheduler ) noexcept
		{
			m_scheduler_ptr = std::addressof( scheduler );
		}
		Exe & get_scheduler() noexcept
		{
			assert( m_scheduler_ptr != nullptr );
			return *m_scheduler_ptr;
		}
	private:
		Exe * m_scheduler_ptr = nullptr;
	};

	template <typename T, typename Exe>
	concept CanGetScheduler = requires( T obj )
	{
		{ obj.get_scheduler() } noexcept -> std::same_as<Exe &>;
	};

	template <typename T, typename Exe>
	concept CanSetScheduler = requires( T obj, Exe scheduler )
	{
		{ obj.set_scheduler( scheduler ) } noexcept -> std::same_as<void>;
	};
}