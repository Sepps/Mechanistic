#pragma once
#include "CoroScheduable.hpp"
#include <coroutine>

namespace Jam
{
	template <typename Exe, CanSetScheduler<Exe> Promise>
	struct Coawaitable
	{
		std::coroutine_handle<Promise> handle;

		[[nodiscard]] bool await_ready() const noexcept
		{
			return false;
		}
		template <CanGetScheduler<Exe> CallerPromise> 
		[[nodiscard]] auto await_suspend( std::coroutine_handle<CallerPromise> caller ) const noexcept -> std::coroutine_handle<Promise>
		{
			auto & promise = handle.promise();
			promise.set_continuation( caller );
			promise.set_scheduler(caller.promise().get_scheduler());
			return handle;
		}
		[[nodiscard]] auto await_resume() noexcept
		{
			return handle.promise().extract_value();
		}
	};
}