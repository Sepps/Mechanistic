#include "jam/MainLoop.hpp"
#include "jam/Window.hpp"
#include "tasks/Input.hpp"
#include "cst/Cst.hpp"
#include <thread>
#include <iostream>

int main()
{
	auto window_offset_rect = Cst::Rect{
		.pt = {
			.x = { 0 },
			.y = { 0 },
		},
		.rect = {
			.width = { 640 },
			.height = { 480 },
		}
	};
	auto value = window_offset_rect.Offset({ 1 }, { 2 }, { 3 }, { 4 });
	// std::thread ui_thread{[&]
	// {
	// 	auto window = Jam::CreateWindow(
    //         Jam::WindowBackend::Sdl2, 
    //         "Mechanistic",
    //         Jam::WindowExtents{
    //             .x = Jam::Centered{},
    //             .y = Jam::Centered{},
    //             .width = static_cast<std::uint32_t>(window_rect.width.value),
    //             .height = static_cast<std::uint32_t>(window_rect.height.value),
    //         },
    //         Jam::WindowSettings{
    //             .type = Jam::WindowType::Normal,
    //             .flags = Jam::WindowFlags{},
    //             .display_state = Jam::WindowDisplayState::Restored,
    //             .graphics = {
    //                 .opengl = true,
    //             }
    //         }
    //     );
	// 	auto scheduler = Jam::Scheduler();
	// 	auto input_pumper = App::InputPumper{ scheduler, window };
	// 	scheduler.Push(input_pumper);
	// 	Jam::MainLoop(scheduler);
	// }};

	// std::thread input_thread{[&]
	// {
	// 	auto scheduler = Jam::Scheduler();
	// 	auto input = App::Input();
	// 	auto input_event = input(scheduler);
	// 	auto keyboard_event = input.keyboard(scheduler);
	// 	auto mouse_event = input.mouse(scheduler);
	// 	scheduler.Push(input_event);
	// 	scheduler.Push(keyboard_event);
	// 	scheduler.Push(mouse_event);
	// 	Jam::OtherLoop(scheduler);
	// }};

	// input_thread.join();
	// ui_thread.join();
}