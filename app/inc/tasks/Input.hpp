#pragma once
#include "jam/Window.hpp"
#include "async/Async.hpp"
#include "async/CoroEntry.hpp"
#include "async/CoroMessageQueue.hpp"
#include "async/Scheduler.hpp"
#include "sdl2/Sdl2KeyEvent.hpp"
#include "sdl2/Sdl2MouseEvent.hpp"

namespace App
{
	struct Keyboard
	{
		[[nodiscard]] auto operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>;

		Jam::MessageQueue<Jam::KeyEvent> keyboard_events;
	};

	struct Mouse
	{
		[[nodiscard]] auto operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>;

		Jam::MessageQueue<Jam::MouseEvent> mouse_events;
	};

	// This must be executed on the thread that a window was created on,
	// only when it is executed can any other thread recieve input.
	struct InputPumper
	{
		void operator()() noexcept;
		Jam::Scheduler & scheduler;
		Jam::Window const & window;
	};

	struct Input
	{
		[[nodiscard]] auto operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>;

		Keyboard keyboard;
		Mouse mouse;
	};
}