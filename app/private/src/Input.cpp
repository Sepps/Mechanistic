#include "tasks/Input.hpp"
#include "async/CoroYield.hpp"
#include "async/Scheduler.hpp"
#include "jam/Window.hpp"
#include "sdl2/Sdl2AppEvent.hpp"
#include "sdl2/Sdl2Event.hpp"
#include "sdl2/Sdl2EventToString.hpp"
#include "sdl2/Sdl2KeyEvent.hpp"
#include "sdl2/Sdl2MouseEvent.hpp"
#include "sdl2/Sdl2WindowEvent.hpp"
#include "sdl2/Sdl2Init.hpp"

#include <iostream>
#include <string>
#include <variant>

namespace App
{
	auto Keyboard::operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>
	{
		for (;;)
		{
			auto opt_result = co_await keyboard_events.AsyncPop();
			if (opt_result)
			{
				auto & result = *opt_result;
				std::cout << "keyboard: " + std::string(ToString(result.key_code)) + "\n";
			}
			else co_await Jam::Yield<>{};
		}
	}

	auto Mouse::operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>
	{
		for (;;)
		{
			auto opt_result = co_await mouse_events.AsyncPop();
			if (opt_result)
			{
				auto & result = *opt_result;
				std::cout << "mouse: " + std::to_string(result.coordinate.x) + ", " + std::to_string(result.coordinate.y) + "\n";
			}
			else co_await Jam::Yield<>{};
		}
	}

	struct Handler
	{
		Jam::MessageQueue<Jam::KeyEvent> & keyboard_events;
		Jam::MessageQueue<Jam::MouseEvent> & mouse_events;

		void operator()(Jam::MouseEvent const & event) const noexcept
		{
			mouse_events.Push(event);
		}
		void operator()(Jam::KeyEvent const & event) const noexcept
		{
			if (event.key_state) keyboard_events.Push(event);
		}
		void operator()(Jam::WindowEvent const & event) const noexcept
		{
			std::visit((*this), event.event_data);
		}
		void operator()(Jam::AppEvent app) const noexcept
		{
			if (app == Jam::AppEvent::Quit)
			{
				std::exit(0);
			}
		}
		void operator()(auto const & event) const noexcept
		{
		}
	};

	void InputPumper::operator()() noexcept
	{
		Jam::PumpEvents();
		scheduler.Push(*this);
	}

	auto Input::operator()(Jam::Scheduler & scheduler) noexcept -> Jam::Entry<>
	{
		for (;;)
		{
			for (auto event : Jam::PollEvents())
			{
				std::visit(Handler{keyboard.keyboard_events, mouse.mouse_events}, event.event_data);
			}
			co_await Jam::Yield{};
		}
	}
}